import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DatasetIndexComponent} from 'src/app/modules/hlt/datasets/dataset-index/dataset-index.component';
import {PathIndexComponent} from 'src/app/modules/hlt/paths/path-index/path-index.component';
import {SeedIndexComponent} from 'src/app/modules/hlt/seeds/seed-index/seed-index.component';
import {PrescaleSummaryComponent} from "src/app/modules/hlt/summaries/prescale-summary/prescale-summary.component";
import {AdminPanelComponent} from 'src/app/modules/security/admin/admin-panel/admin-panel.component';
import {AppComponent} from './app.component';
import {RunsSummaryComponent} from './modules/hlt/runs/summary/runs-summary.component';
import {LoginComponent} from './modules/security/user/login/login.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {Error401Component} from './pages/error401/error401.component';
import {Error404Component} from './pages/error404/error404.component';
import {SessionAccessManager} from './services/session-access-manager.service';

// PAGES
const routes: Routes = [
    // PAGES
    {
        path: '401',
        component: Error401Component,
        data: {
            showMenu: false,
        }
    },
    {
        path: '404',
        component: Error404Component,
        data: {
            showMenu: false,
        }
    },

    // LOGIN
    {
        path: 'login',
        component: LoginComponent,
        canActivate: [SessionAccessManager],
        data: {
            permittedRoles: ['no-user'],
            error401URL: ''
        }
    },

    // DASHBOARD
    {
        path: '',
        component: DashboardComponent,
        canActivate: [SessionAccessManager],
        data: {
            permittedRoles: ['user-token']
        }
    },

    // MODULES
    // AVAILBLE ROLES:
    // HLT_USER: WRITE COMMENTS ON PATHS
    //
    {
        path: 'security/admin',
        component: AdminPanelComponent,
        canActivate: [SessionAccessManager],
        data: {
            permittedRoles: ['admin']
        }
    },
    {
        path: 'runs/summary',
        component: RunsSummaryComponent,
        canActivate: [SessionAccessManager],
        data: {
            permittedRoles: ['user-token']
        }
    },
    {
        path: 'datasets',
        component: DatasetIndexComponent,
        canActivate: [SessionAccessManager],
        data: {
            permittedRoles: ['user-token']
        }
    },
    {
        path: 'paths',
        component: PathIndexComponent,
        canActivate: [SessionAccessManager],
        data: {
            permittedRoles: ['user-token']
        }
    },
    {
        path: 'seeds',
        component: SeedIndexComponent,
        canActivate: [SessionAccessManager],
        data: {
            permittedRoles: ['user-token']
        }
    },
    {
        path: 'summaries/prescales',
        component: PrescaleSummaryComponent,
        canActivate: [SessionAccessManager],
        data: {
            permittedRoles: ['user-token']
        }
    },

    // OLD
    {
        path: '',
        redirectTo: '',
        pathMatch: 'full'
    },
    {
        path: '',
        component: AppComponent,
        data: {
            breadcrumb: 'app',
        }
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
