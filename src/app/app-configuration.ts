import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class AppConfiguration {

    static root: any = {};

    static async load(http: HttpClient): Promise<void> {
        const jsonFile = 'assets/config/config.json';

        await http.get(jsonFile)
            .toPromise()
            .then((response: any) => {
                AppConfiguration.root = response;
            })
            .catch((response: any) => {
                console.log(`Could not load file '${jsonFile}': ${JSON.stringify(response)}`);
            });
    }

}
