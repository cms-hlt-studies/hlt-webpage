import {Injectable} from '@angular/core';

// SERVICES
import {ApiService} from 'src/app/services/api.service';

export class SessionCache {

    type: string;
    data: any;

    constructor(id: string) {
        // TYPE
        this.type = id ? 'user' : 'anonymous';

        // READ CACHE
        const cacheJson = localStorage.getItem(this.type);

        if (cacheJson) {
            const cache = JSON.parse(cacheJson);

            if (this.type === 'user') {
                if (cache.id === id) {
                    this.data = cache;
                } else {
                    this.data = {id};
                }
            } else {
                this.data = cache;
            }
        } else {
            if (this.type === 'user') {
                this.data = {id};
            } else {
                this.data = {};
            }
        }

        // SAVE
        localStorage.setItem(this.type, JSON.stringify(this.data));
    }

    getItem(key: string): any {
        return this.data[key];
    }

    setItem(key: string, value: any): void {
        this.data[key] = value;
        localStorage.setItem(this.type, JSON.stringify(this.data));
    }

}

@Injectable({
    providedIn: 'root'
})
export class Session {

    // SYNC
    resync: boolean;

    // PROFILE
    profile: any;
    roles: Array<any>;

    // CACHE
    cache: SessionCache;

    constructor(
        private apiService: ApiService
    ) {
        // UPDATE API HEADERS
        this.updateAPIHeaders();

        // SYNC
        this.resync = true;

        // PROFILE
        this.profile = null;
        this.roles = [];

        // CACHE
        this.cache = new SessionCache(null);

        // INTERCEPTORS
        this.apiService.addInterceptor((response: Response) => {
            if (response.headers.has('HLT-UTKN')) {
                this.refreshToken(response.headers.get('HLT-UTKN'));
            }
        });
    }

    get authenticated(): boolean {
        return !this.resync && !!localStorage.getItem('token');
    }

    setToken(token: string): void {
        // RESET
        this.profile = null;
        this.roles = [];
        this.resync = false;

        // SAVE TOKEN
        localStorage.setItem('token', token);

        // UPDATE API HEADERS
        this.updateAPIHeaders();

        // RESYNC
        this.resync = true;
    }

    refreshToken(token: string): void {
        // SAVE TOKEN
        localStorage.setItem('token', token);

        // UPDATE API HEADERS
        this.updateAPIHeaders();
    }

    sync(): Promise<void> {
        return new Promise((resolve, reject) => {
            if (this.resync) {
                this.apiService.get(
                    'security/user/profile'
                )
                    .then(res => {
                        // PROFILE
                        this.profile = {...res};
                        this.roles = res.roles || [];

                        // DON'T SYNC
                        this.resync = false;

                        // CACHE
                        this.cache = new SessionCache(
                            this.profile.user || this.profile['user-token']
                        );

                        // RESOLVE
                        resolve(undefined);
                    })
                    .catch(error => {
                        // PROFILE
                        this.profile = null;
                        this.roles = [];

                        // REMOVE TOKEN
                        localStorage.removeItem('token');

                        // UPDATE API HEADERS
                        this.updateAPIHeaders();

                        // SYNC
                        this.resync = true;

                        // CACHE
                        this.cache = new SessionCache(null);

                        // REJECT
                        reject(error);
                    });
            } else {
                // RESOLVE
                resolve(undefined);
            }
        });
    }

    updateAPIHeaders(): void {
        const token = localStorage.getItem('token');

        if (token) {
            this.apiService.baseHeaders = () => ({
                Authorization: 'HLT-UTKN ' + token
            });
        } else {
            this.apiService.baseHeaders = () => ({});
        }
    }

    hasRoles(permittedRoles: Array<string>): boolean {
        let hasRoles = false;

        for (const role of permittedRoles) {
            for (const ownedRole of this.roles) {
                if (role.toLowerCase() === ownedRole.toLowerCase()) {
                    hasRoles = true;
                    break;
                }
            }

            if (hasRoles) {
                break;
            }
        }

        return hasRoles;
    }

}
