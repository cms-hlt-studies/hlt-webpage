import {Location} from '@angular/common';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';

export function getParamMap(route: ActivatedRoute): Promise<ParamMap> {
    let queryParamsSubscription;

    return new Promise<ParamMap>((resolve, reject) => {
        queryParamsSubscription = route.queryParamMap.subscribe(params => {
            resolve(params);
        });
    })
        .finally(() => {
            queryParamsSubscription.unsubscribe();
        });
}


export class Path {

    public queryParams: Observable<any>;
    private queryParamValues: any;
    private queryParamsSubscription: Subscription;
    private observers: Array<any>;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private location: Location
    ) {
        // QUERY PARAMS
        this.queryParamValues = {};

        this.observers = [];

        this.queryParams = new Observable((observer) => {
            // ADD OBSERVER
            this.observers.push(observer);

            // FORWARD PARAMS
            observer.next(this.queryParamValues);
        });

        this.queryParamsSubscription = route.queryParams.subscribe(params => {
            // SAVE QUERY PARAMS
            this.queryParamValues = {...params};

            // FIRE CHANGES
            this.fireParameterChange();
        });
    }

    public destroy(): void {
        this.queryParamsSubscription.unsubscribe();

        this.route = null;
        this.router = null;
        this.location = null;

        this.queryParamValues = null;
        this.observers = null;

        this.queryParams = null;
    }

    public getLocationPath(): string {
        return this.location.path();
    }

    public clearParameters(): void {
        this.queryParamValues = {};
        this.updatePath();
    }

    public hasParameter(key: string): boolean {
        return key in this.queryParamValues;
    }

    public getParameter(key: string): string {
        return this.queryParamValues[key];
    }

    public unsetParameter(key: string): void {
        const value = this.queryParamValues[key];

        if (value !== undefined) {
            this.queryParamValues[key] = undefined;
            this.updatePath();
        }
    }

    public setParameter(key: string, newValue: any): void {
        const value = this.queryParamValues[key];

        if (value !== newValue) {
            this.queryParamValues[key] = newValue;
            this.updatePath();
        }
    }

    private updatePath(): void {
        // CLONE QUERY PARAM VALUES
        const queryParams = {...this.queryParamValues};

        // UPDATE PATH
        if (Object.keys(queryParams).length > 0) {
            const url = this.router
                .createUrlTree([], {relativeTo: this.route, queryParams})
                .toString();

            this.location.go(url);
        } else {
            const url = this.router
                .createUrlTree([], {relativeTo: this.route})
                .toString();

            this.location.go(url);
        }
    }

    private fireParameterChange(): void {
        // CLONE QUERY PARAM VALUES
        const queryParams = {...this.queryParamValues};

        // FIRE
        this.observers.forEach((observer) => {
            observer.next(queryParams);
        });
    }

}
