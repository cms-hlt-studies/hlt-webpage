import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {vp} from '@smmx/maria-commons';
import {AppConfiguration} from 'src/app/app-configuration';
import {Session} from 'src/app/services/session.service';
import {UIControl} from "src/app/services/ui-control.service";
import {getParamMap} from 'src/app/utils/paths';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

    // INPUTS
    token: string;
    returnUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private session: Session,
        private uiControl: UIControl,
    ) {
        // ANGULAR
        this.route = route;
        this.router = router;

        // SERVICES
        this.session = session;

        // UI
        this.uiControl.viewMode = 'default';
    }

    ngOnInit(): void {
        // URL ARGS
        this.token = null;
        this.returnUrl = null;

        getParamMap(this.route)
            .then(params => {
                if (params.has('returnUrl')) {
                    this.returnUrl = vp()
                        .asString()
                        .notNull()
                        .apply(params.get('returnUrl'));
                }

                if (params.has('token')) {
                    this.token = vp()
                        .asString()
                        .notNull()
                        .apply(params.get('token'));
                }
            })
            .then(() => {
                if (this.token) {
                    this.session.setToken(this.token);

                    // REDIRECT
                    this.redirectToRequestedPage();
                } else {
                    setTimeout(() => {
                        this.redirectToSSO();
                    }, 5000);
                }
            });
    }

    redirectToSSO(): void {
        const searchParams = new URLSearchParams({
            callback: window.location.href,
        });

        window.location.href = AppConfiguration.root.cernSSO.url
            + '?' + searchParams.toString();
    }

    redirectToRequestedPage(): void {
        if (this.returnUrl) {
            this.router.navigateByUrl(this.returnUrl);
        } else {
            this.router.navigate(['']);
        }
    }

}
