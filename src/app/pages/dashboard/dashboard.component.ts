import {Clipboard} from '@angular/cdk/clipboard';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Form, form, vp} from '@smmx/maria-commons';
import {MessageService} from 'primeng/api';
import {ApiService} from 'src/app/services/api.service';
import {ResultsConfig} from 'src/app/services/results-config.service';
import {UIControl} from 'src/app/services/ui-control.service';
import {CountdownLatch} from 'src/app/utils/countdown-latch';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit, OnDestroy {

    searchForm: Form;
    datasetResults: [];
    pathResults: [];
    seedResults: [];

    datasetLoading;
    pathLoading;
    seedLoading;

    commonDatasetQueries: [string, string, string];
    commonPathQueries: [string, string, string];
    commonSeedQueries: [string, string, string];

    // RESULTS SUBSCRIPTION
    resultsConfigUnsubscribe: any;

    constructor(
        private router: Router,
        private resultsConfig: ResultsConfig,
        private apiService: ApiService,
        private clipboard: Clipboard,
        private messageService: MessageService,
        private uiControl: UIControl,
    ) {
        this.uiControl.viewMode = 'catalog';

        this.messageService = messageService;
        this.datasetResults = [];
        this.pathResults = [];
        this.seedResults = [];
        this.datasetLoading = 0;
        this.seedLoading = 0;
        this.pathLoading = 0;
        this.searchForm = form()
            .registerField(
                'query',
                vp()
                    .asString()
                    .notNull()
            );

        this.commonDatasetQueries = ['DoubleMuon', 'SingleMuon', '*EG*'];
        this.commonPathQueries = ['HLT*', '*Mu*', '*EG*'];
        this.commonSeedQueries = ['L1*', '*Mu*', '*EG*'];

        // SUBSCRIBE TO SEARCH CONFIG
        this.resultsConfigUnsubscribe = resultsConfig.addListener((field, oldValue, newValue) => {
            if (field === 'year') {
                this.submitSearchQuery({key: 'Enter'});
            }
        });
    }

    ngOnInit(): void {
        this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: 'Welcome!'
        });
    }

    ngOnDestroy(): void {
        this.resultsConfigUnsubscribe();
    }

    submitCommonQuery(query: string): void {
        this.searchForm.setValue('query', query);
        this.submitSearchQuery({key: 'Enter'});
    }

    submitSearchQuery(event: any): void {
        if (event.key !== 'Enter') {
            return;
        }

        // CLEARING OLD RESULTS
        this.datasetResults = [];
        this.pathResults = [];
        this.seedResults = [];
        this.datasetLoading = 0.1;
        this.seedLoading = 0.1;
        this.pathLoading = 0.1;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // DATASETS
        const datasetCL = new CountdownLatch(2);
        const datasetResultsTemp = [];

        // SEARCH DATASETS BY NAME
        this.apiService.get(
            'hlt/datasets/', {
                _pOffset: 0,
                _pLimit: -1,
                year: this.resultsConfig.getValue('year'),
                query: '*' + this.searchForm.getField('query').value + '*',
            }
        )
            .then(res => {
                for (const entry of res.entries) {
                    // @ts-ignore
                    datasetResultsTemp.push(entry);
                }
            })
            .finally(() => {
                datasetCL.countDown();
            });

        // SEARCH DATASETS BY TAG
        this.apiService.get(
            'hlt/datasets/', {
                _pOffset: 0,
                _pLimit: -1,
                year: this.resultsConfig.getValue('year'),
                tag: '*' + this.searchForm.getField('query').value + '*',
            }
        )
            .then(res => {
                for (const entry of res.entries) {
                    // @ts-ignore
                    datasetResultsTemp.push(entry);
                }
            })
            .finally(() => {
                datasetCL.countDown();
            });

        datasetCL.then(() => {
            // @ts-ignore
            this.datasetResults = this.remDuplicates(datasetResultsTemp);
            this.datasetLoading = 1;
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // PATHS
        const pathCL = new CountdownLatch(2);
        const pathResultsTemp = [];

        // SEARCH PATHS BY NAME
        this.apiService.get(
            'hlt/paths/', {
                _pOffset: 0,
                _pLimit: -1,
                year: this.resultsConfig.getValue('year'),
                query: '*' + this.searchForm.getField('query').value + '*'
            }
        )
            .then(res => {
                for (const entry of res.entries) {
                    // @ts-ignore
                    pathResultsTemp.push(entry);
                }
            })
            .finally(() => {
                pathCL.countDown();
            });

        // SEARCH PATHS BY TAG
        this.apiService.get(
            'hlt/paths/', {
                _pOffset: 0,
                _pLimit: -1,
                year: this.resultsConfig.getValue('year'),
                tag: '*' + this.searchForm.getField('query').value + '*'
            }
        )
            .then(res => {
                for (const entry of res.entries) {
                    // @ts-ignore
                    pathResultsTemp.push(entry);
                }
            })
            .finally(() => {
                pathCL.countDown();
            });

        // SEARCH PATHS BY SEED
        this.apiService.get(
            'hlt/paths/', {
                _pOffset: 0,
                _pLimit: -1,
                year: this.resultsConfig.getValue('year'),
                seed: '*' + this.searchForm.getField('query').value + '*'
            }
        )
            .then(res => {
                for (const entry of res.entries) {
                    // @ts-ignore
                    pathResultsTemp.push(entry);
                }
            })
            .finally(() => {
                pathCL.countDown();
            });

        pathCL.then(() => {
            // @ts-ignore
            this.pathResults = this.remDuplicates(pathResultsTemp);
            this.pathLoading = 1;
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // SEEDS
        const seedCL = new CountdownLatch(1);
        const seedResultsTemp = [];

        // SEARCH SEEDS BY NAME
        this.apiService.get(
            'l1t/seeds', {
                _pOffset: 0,
                _pLimit: -1,
                year: this.resultsConfig.getValue('year'),
                query: '*' + this.searchForm.getField('query').value + '*'
            }
        )
            .then(res => {
                for (const entry of res.entries) {
                    // @ts-ignore
                    seedResultsTemp.push(entry);
                }
            })
            .finally(() => {
                seedCL.countDown();
            });

        seedCL.then(() => {
            // @ts-ignore
            this.seedResults = this.remDuplicates(seedResultsTemp);
            this.seedLoading = 1;
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }

    pathRedirect(hltPath: string): void {
        const url = 'paths';
        this.router.navigate([url], {queryParams: {sMethod: 'name', sQuery: hltPath}});
    }

    datasetRedirect(dataset: string): void {
        const url = 'datasets';
        this.router.navigate([url], {queryParams: {sMethod: 'name', sQuery: dataset}});
    }

    getBackground(index: any): string {
        if (index % 2 === 0) {
            return 'rgba(0, 73, 104, .3)';
        }
        return 'rgba(0, 73, 104, .6)';
    }

    copyToClipboard(text: string): void {
        this.clipboard.copy(text);
        this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: 'Copied to Clipboard!'
        });
    }

    remDuplicates(original): any[] {
        if (!original) {
            return [];
        }

        // SORT
        original.sort((selected, compare) => {
            if (selected.name > compare.name) {
                return 1;
            } else if (selected.name < compare.name) {
                return -1;
            }

            return 0;
        });

        // INIT
        const unique = [];

        // FIND
        let previous = null;

        original.forEach((entry) => {
            if (!previous) {
                unique.push(entry);
            } else if (previous.name !== entry.name) {
                unique.push(entry);
            }

            previous = entry;
        });

        // RETURN
        return unique;
    }

}
