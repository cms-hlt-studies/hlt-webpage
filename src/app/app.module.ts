import {ClipboardModule} from '@angular/cdk/clipboard';
import {FullscreenOverlayContainer, OverlayContainer, OverlayModule} from '@angular/cdk/overlay';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AccordionModule} from 'primeng/accordion';
import {MessageService} from 'primeng/api';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {BlockUIModule} from 'primeng/blockui';
import {CalendarModule} from 'primeng/calendar';
import {CheckboxModule} from 'primeng/checkbox';
import {DropdownModule} from 'primeng/dropdown';
import {DialogService, DynamicDialogModule} from 'primeng/dynamicdialog';
import {ListboxModule} from 'primeng/listbox';
import {MessageModule} from 'primeng/message';
import {MessagesModule} from 'primeng/messages';
import {PaginatorModule} from 'primeng/paginator';
import {ProgressBarModule} from 'primeng/progressbar';
import {SidebarModule} from 'primeng/sidebar';
import {SliderModule} from 'primeng/slider';
import {TableModule} from 'primeng/table';
import {TabViewModule} from 'primeng/tabview';
import {ToastModule} from 'primeng/toast';
import {TooltipModule} from 'primeng/tooltip';
import {FormDialogComponent} from 'src/app/components/form-dialog/form-dialog.component';
import {FormComponent} from 'src/app/components/form/form.component';
import {ListComponent} from 'src/app/components/list/list.component';
import {PrescaleSummaryComponent} from 'src/app/modules/hlt/summaries/prescale-summary/prescale-summary.component';
import {DialogComponent} from 'src/app/services/dialog/dialog.component';
import {AppConfiguration} from './app-configuration';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CatalogComponent} from './components/catalog/catalog.component';
import {ConfirmationDialogComponent} from './components/confirmation-dialog/confirmation-dialog.component';
import {DatasetIndexComponent} from './modules/hlt/datasets/dataset-index/dataset-index.component';
import {DatasetViewComponent} from './modules/hlt/datasets/dataset-view/dataset-view.component';
import {PathIndexComponent} from './modules/hlt/paths/path-index/path-index.component';
import {PathViewComponent} from './modules/hlt/paths/path-view/path-view.component';
import {RunsSummaryComponent} from './modules/hlt/runs/summary/runs-summary.component';
import {SeedIndexComponent} from './modules/hlt/seeds/seed-index/seed-index.component';
import {SeedViewComponent} from './modules/hlt/seeds/seed-view/seed-view.component';
import {AdminPanelComponent} from './modules/security/admin/admin-panel/admin-panel.component';
import {LoginComponent} from './modules/security/user/login/login.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {Error401Component} from './pages/error401/error401.component';
import {Error404Component} from './pages/error404/error404.component';
import {SessionAccessManager} from './services/session-access-manager.service';
import {Session} from './services/session.service';


// INTIALIZER
export function initializeApp(httpClient: HttpClient): () => Promise<void> {
    return async () => {
        // LOAD CONFIG
        await AppConfiguration.load(httpClient);
    };
}

// ANGULAR
@NgModule({
    declarations: [
        AppComponent,

        // COMPONENTS
        CatalogComponent,
        FormComponent,
        FormDialogComponent,
        ListComponent,

        // PAGES
        Error401Component,
        Error404Component,

        LoginComponent,
        DashboardComponent,
        DialogComponent,
        ConfirmationDialogComponent,
        AdminPanelComponent,
        DatasetIndexComponent,
        DatasetViewComponent,
        PathIndexComponent,
        PathViewComponent,
        SeedIndexComponent,
        SeedViewComponent,
        RunsSummaryComponent,
        PrescaleSummaryComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        OverlayModule,
        ClipboardModule,

        AccordionModule,
        AutoCompleteModule,
        BlockUIModule,
        CalendarModule,
        CheckboxModule,
        DropdownModule,
        DynamicDialogModule,
        ListboxModule,
        MessagesModule,
        MessageModule,
        PaginatorModule,
        ProgressBarModule,
        SidebarModule,
        SliderModule,
        TableModule,
        TabViewModule,
        ToastModule,
        TooltipModule,
    ],
    providers: [
        Session,
        SessionAccessManager,
        DialogService,
        MessageService,
        {provide: OverlayContainer, useClass: FullscreenOverlayContainer},
        {
            provide: APP_INITIALIZER,
            useFactory: initializeApp,
            deps: [HttpClient],
            multi: true
        }
    ],
    entryComponents: [
        ConfirmationDialogComponent,
        FormDialogComponent,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
