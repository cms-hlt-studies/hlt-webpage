import {Injectable} from '@angular/core';
import {Session} from 'src/app/services/session.service';

@Injectable({
    providedIn: 'root'
})
export class ResultsConfig {

    listeners: Array<any>;

    constructor(
        private session: Session
    ) {
        // INIT LISTENERS
        this.listeners = [];

        // INIT CONFIG
        const config = this.config();

        // INIT YEAR
        if (!config.year) {
            config.year = 2018;
        }

        // SET
        this.session.cache.setItem('results', config);
    }

    config(): any {
        let config = this.session.cache.getItem('results');

        // INIT GLOBAL
        if (!config) {
            config = {
                year: 2018
            };
        }

        return config;
    }

    addListener(listener: (field: string, oldValue: any, newValue: any) => void): () => void {
        this.listeners.push(listener);

        return () => {
            const index = this.listeners.indexOf(listener, 0);

            if (index > -1) {
                this.listeners.splice(index, 1);
            }
        };
    }

    setValue(field: string, value: any): void {
        const config = this.config();
        const oldValue = config[field];

        // SHORT-CIRCUIT
        if (oldValue === undefined) {
            return;
        }

        // UPDATE VALUE
        config[field] = value;

        // UPDATE CONFIG
        this.session.cache.setItem('results', config);

        // FIRE LISTENERS
        if (oldValue !== value) {
            this.listeners.forEach(listener => {
                listener(field, oldValue, value);
            });
        }
    }

    getValue(field: string): any {
        const config = this.config();

        return config[field];
    }

}
