import {Location} from '@angular/common';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {form, Form, vp} from '@smmx/maria-commons';
import BigNumber from 'bignumber.js';
import {MessageService} from 'primeng';
import {ApiService} from 'src/app/services/api.service';
import {ResultsConfig} from 'src/app/services/results-config.service';
import {Session} from 'src/app/services/session.service';
import {UIControl} from 'src/app/services/ui-control.service';
import {Path} from 'src/app/utils/paths';

@Component({
    selector: 'app-dataset-index',
    templateUrl: './dataset-index.component.html',
    styleUrls: ['./dataset-index.component.less']
})
export class DatasetIndexComponent implements OnInit, OnDestroy {

    // ID
    componentId: string;

    // SERVICES
    path: Path;

    // INDEX
    datasets: Array<Array<any>>;
    datasetsTable: any;
    datasetsEvent: any;
    datasetsLoading: boolean;
    datasetsTotalRecords: number;
    getIndexPromise: any;
    datasetData: any;

    // SEARCH
    searchMethods: any[];
    searchMethod: number;
    searchForms: Form[];

    // RESULTS SUBSCRIPTION
    resultsConfigUnsubscribe: any;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private resultsConfig: ResultsConfig,
        private location: Location,
        private apiService: ApiService,
        private messageService: MessageService,
        public session: Session,
        private uiControl: UIControl,
    ) {
        // ID
        this.componentId = '1bf8850aa96242d1977420ac3b4c0727';

        // UI
        this.uiControl.viewMode = 'catalog';

        // SERVICES
        this.path = new Path(route, router, location);

        // INDEX
        this.datasets = [];
        this.datasetsTable = {
            fields: [
                {field: 'name', header: 'Name', sortable: true},
                {field: 'years', header: 'Years'},
                {field: 'intlumi_actual', header: 'Actual [fb^-1]', sortable: true},
                {field: 'intlumi_effective', header: 'Effective [fb^-1]', sortable: true},
                {field: '__options__', header: 'Options', sortable: false},
            ],
            data: []
        };
        this.datasetsEvent = null;
        this.datasetsLoading = false;
        this.datasetsTotalRecords = 0;
        this.getIndexPromise = null;

        // SELECTED
        this._dataset = null;
        this.datasetData = null;

        // SEARCH
        this.searchMethods = [
            {label: 'By Name', value: 0},
            {label: 'By Tag', value: 1},
        ];

        this.searchMethod = 0;

        this.searchForms = [
            form()
                .registerField(
                    'query',
                    vp()
                        .asString()
                        .notNull(),
                    '*'
                ),
            form()
                .registerField(
                    'tag',
                    vp()
                        .asString()
                        .notNull()
                ),
        ];

        // SUBSCRIBE TO SEARCH CONFIG
        this.resultsConfigUnsubscribe = resultsConfig.addListener((field, oldValue, newValue) => {
            if (field === 'year') {
                this.refreshIndex();
            }
        });
    }

    // SELECTED
    _dataset: string;

    get dataset(): string {
        return this._dataset;
    }

    set dataset(dataset: string) {
        this._dataset = dataset;

        if (dataset) {
            // URL
            this.path.setParameter('select', dataset);

            // DATA
            this.datasetData = null;

            this.apiService.get(
                'hlt/datasets/' + dataset
            )
                .then(res => {
                    this.datasetData = {...res};
                })
                .catch(err => {
                    this.datasetData = null;
                });
        } else {
            // URL
            this.path.unsetParameter('select');

            // DATA
            this.datasetData = null;
        }
    }

    ngOnInit(): void {
        // URL ARGS
        this.path.queryParams.subscribe(params => {
            if ('sMethod' in params) {
                const sMethod = vp()
                    .asString()
                    .notNull()
                    .map((method) => {
                        if (method === 'name') {
                            return 0;
                        } else if (method === 'tag') {
                            return 1;
                        } else {
                            return 0;
                        }
                    })
                    .apply(params.sMethod);

                this.searchMethod = sMethod;
            }

            if ('sQuery' in params) {
                const sQuery = vp()
                    .asString()
                    .notNull()
                    .apply(params.sQuery);

                this.searchForms[0].getField('query').value = sQuery;
            }

            if ('sTag' in params) {
                const sTag = vp()
                    .asString()
                    .notNull()
                    .apply(params.sTag);

                this.searchForms[1].getField('tag').value = sTag;
            }

            // REFRESH
            this.refreshIndex();

            // ITEM
            if ('select' in params) {
                const select = vp()
                    .asString()
                    .notNull()
                    .apply(params.select);

                this.getIndexPromise.then((items) => {
                    this.dataset = select;
                });
            }
        });
    }

    ngOnDestroy(): void {
        this.path.destroy();
        this.resultsConfigUnsubscribe();
    }

    refreshIndex(event = null): void {
        // CACHE PREVIOUS EVENT
        if (!event) {
            event = this.datasetsEvent;
        } else {
            this.datasetsEvent = event;
        }

        // RESET
        this.datasetsTable.data = [];

        // BUILD SFIELDS
        const sFields = [];

        if (event && event.multiSortMeta) {
            for (const meta of event.multiSortMeta) {
                let order;

                if (meta.order === 1) {
                    order = 'ASC';
                } else {
                    order = 'DESC';
                }

                sFields.push(meta.field.replace('_view', '') + ':' + order);
            }
        }

        // BUILD QUERY
        this.path.unsetParameter('sMethod');
        this.path.unsetParameter('sQuery');
        this.path.unsetParameter('sSeed');
        this.path.unsetParameter('sTag');

        const request: any = {
            _pOffset: event ? event.first : 0,
            _pLimit: event ? event.rows : 50,
            _sFields: sFields,
            year: this.resultsConfig.getValue('year')
        };

        if (this.searchMethod === 0) {
            request.query = this.searchForms[0].getValue('query');

            // SHORT-CIRCUIT
            if (!this.searchForms[0].allValid) {
                return;
            }

            // QUERY
            this.path.setParameter('sMethod', 'name');
            this.path.setParameter('sQuery', request.query);
        } else if (this.searchMethod === 1) {
            request.tag = this.searchForms[1].getValue('tag');

            // SHORT-CIRCUIT
            if (!this.searchForms[1].allValid) {
                return;
            }

            // QUERY
            this.path.setParameter('sMethod', 'tag');
            this.path.setParameter('sTag', request.tag);
        }

        // FETCH
        this.datasetsLoading = true;

        this.getIndexPromise = this.apiService.get(
            'hlt/datasets',
            request
        )
            .then(res => {
                this.datasets = res.entries;

                this.datasetsTable.data = res.entries.map((entry => {
                    entry.intlumi_actual = new BigNumber(entry.intlumi_actual);
                    entry.intlumi_effective = new BigNumber(entry.intlumi_effective);
                    return entry;
                }));

                if (event) {
                    if (this.datasets.length === event.rows) {
                        this.datasetsTotalRecords = event.first + event.rows * 2;
                    }
                } else {
                    this.datasetsTotalRecords = 100;
                }

                return this.datasets;
            })
            .finally(() => {
                this.datasetsLoading = false;
            });
    }

    refreshSelected(): void {
        const tmp = this.dataset;
        this.dataset = null;

        setTimeout(() => {
            this.dataset = tmp;
        }, 100);
    }

    showDetails(dataset: string): void {
        this.dataset = dataset;
    }

}
