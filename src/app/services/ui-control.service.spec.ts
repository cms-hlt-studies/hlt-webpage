import {TestBed} from '@angular/core/testing';

import {UIControl} from 'src/app/services/ui-control.service';

describe('uiConfigService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: UIControl = TestBed.get(UIControl);
        expect(service).toBeTruthy();
    });
});
