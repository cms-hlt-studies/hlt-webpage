import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Form} from '@smmx/maria-commons';
import {DialogService, DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {FormComponent} from 'src/app/components/form/form.component';

@Component({
    selector: 'app-form-dialog',
    templateUrl: './form-dialog.component.html',
    styleUrls: ['./form-dialog.component.less'],
    providers: [
        DialogService
    ],
})
export class FormDialogComponent implements AfterViewInit, OnInit {

    // FORM
    @ViewChild(FormComponent)
    formComponent: FormComponent;
    formReady: boolean;
    formModel: any;

    constructor(
        private dialogRef: DynamicDialogRef,
        public dialogConfig: DynamicDialogConfig
    ) {
        // FORM
        this.formReady = false;
        this.formModel = dialogConfig.data.model;
    }

    get form(): Form {
        // SHORT-CIRCUIT
        if (!this.formReady) {
            return null;
        }

        return this.formComponent.form;
    }

    get valid(): boolean {
        // SHORT-CIRCUIT
        if (!this.formReady) {
            return false;
        }

        // CHECK INPUTS
        if (this.dialogConfig.data.validator) {
            return this.dialogConfig.data.validator(
                this.form
            );
        } else if (!this.form.areAllFieldsValid()) {
            return false;
        }

        // RETURN
        return true;
    }

    ngOnInit(): void {

    }

    ngAfterViewInit(): void {
        this.formReady = true;

        if (this.dialogConfig.data.listener) {
            this.form.addListener((field, oldInput, newInput) => {
                this.dialogConfig.data.listener(
                    this.form,
                    field,
                    oldInput,
                    newInput
                );
            });
        }
    }

    submit(): void {
        // VALIDATION
        if (!this.valid) {
            return;
        }

        // GET DATA
        let data;

        if (this.dialogConfig.data.reader) {
            data = this.dialogConfig.data.reader(
                this.form
            );
        } else {
            data = this.form.getAllValues();
        }

        // LOCK SCREEN
        this.dialogRef.close(data);
    }

    close(): void {
        this.dialogRef.close();
    }

}
