import {Clipboard} from '@angular/cdk/clipboard';
import {Location} from '@angular/common';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Form, form, vp} from '@smmx/maria-commons';
import {Subscription} from 'rxjs';
import {ApiService} from 'src/app/services/api.service';
import {UIControl} from 'src/app/services/ui-control.service';
import {Path} from 'src/app/utils/paths';

@Component({
    selector: 'app-summary',
    templateUrl: './runs-summary.component.html',
    styleUrls: ['./runs-summary.component.less']
})
export class RunsSummaryComponent implements OnInit, OnDestroy {

    path: Path;

    form: Form;

    queryParamsSubscription: Subscription;

    runNumber: string;
    startTime: string;
    stopTime: string;
    achitecture: string;
    hltPaths: any[];
    hltConfig: any[];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private apiService: ApiService,
        private clipboard: Clipboard,
        private uiControl: UIControl,
    ) {
        // UI
        this.uiControl.viewMode = 'default';

        this.path = new Path(route, router, location);

        this.form = form()
            .registerField(
                'run',
                vp()
                    .asString()
                    .notNull()
            );
    }

    copyToClipboard(text: string): void {
        this.clipboard.copy(text);
    }

    ngOnInit(): void {
        // URL ARGS
        this.path.queryParams.subscribe(params => {
            if ('run' in params) {
                const run = vp()
                    .asString()
                    .notNull()
                    .apply(params.run);

                if (run) {
                    // UPDATE FORM
                    this.form.init('run', run);

                    // SEARCH
                    this.searchPaths(run);
                }
            }
        });
    }

    ngOnDestroy(): void {
        this.path.destroy();
    }

    searchPaths(run: string): void {
        // UPDATE PARAMETER
        this.path.setParameter('run', run);

        // LOCK SCREEN
        const unlock = this.uiControl.lockScreen();

        // SUBMIT
        this.apiService.get(
            'cms/runs/summary',
            {run}
        )
            .then(res => {
                this.runNumber = run;
                this.startTime = res.starttime;
                this.stopTime = res.stoptime;
                this.hltPaths = res.paths;
                this.achitecture = res.hlt_arch + ' | ' + res.hlt_sw;
                this.hltConfig = ['Menu: ' + res.hlt_menu, 'Trigger Mode: ' + res.trigger_mode,
                    'L1 Conf Key: ' + res.l1_conf_key, 'L1 RS Key: ' + res.l1_rs_key];
                // this.seeds = res.seeds;
            })
            .finally(() => {
                unlock();
            });
    }

    submit(): void {
        // RESET
        this.runNumber = '';
        this.startTime = '';
        this.stopTime = '';
        this.hltPaths = [];
        this.achitecture = '';
        this.hltConfig = [];

        // CHECKS
        if (!this.form.areAllFieldsValid()) {
            return;
        }

        // GET FIELDS
        const data = this.form.getAllValues();

        // SUBMIT
        this.searchPaths(data.run);
    }

    pathRedirect(hltPath: string): void {
        const url = 'paths/summary';
        this.router.navigate([url], {queryParams: {path: hltPath}});
    }

}
