import {Component, Input, OnInit} from '@angular/core';
import {form, Form} from '@smmx/maria-commons';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.less']
})
export class FormComponent implements OnInit {

    // INPUTS
    form: Form;

    constructor() {
        this._model = {
            fields: []
        };
    }

    // MODEL
    _model: any;

    get model(): any {
        return this._model;
    }

    @Input()
    set model(model) {
        // SET
        this._model = model;

        // BUILD FORM
        this.form = form();

        for (const field of model.fields) {
            this.form.registerField(
                field.name,
                field.valueProcessor,
                field.initValue || null,
            );
        }
    }

    ngOnInit(): void {

    }

}
