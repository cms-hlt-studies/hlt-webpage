import {Component, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {ResultsConfig} from 'src/app/services/results-config.service';
import {Session} from 'src/app/services/session.service';
import {UIControl} from 'src/app/services/ui-control.service';
import {Toggle} from 'src/app/utils/toggle';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent {

    title = 'HLT Webpage';
    params: any;

    // TOOLS
    sidebarToggle: Toggle;

    constructor(
        private router: Router,
        private resultsConfig: ResultsConfig,
        private session: Session,
        public uiControl: UIControl,
    ) {
        this.sidebarToggle = new Toggle(false, 'Active', 'Disabled');
    }

    get isCatalogView(): boolean {
        return this.uiControl.viewMode === 'catalog';
    }

    get authenticated(): boolean {
        return this.session.authenticated;
    }

    get isAdmin(): boolean {
        if (!this.session.roles) {
            return false;
        }

        return this.session.roles.indexOf('admin') > -1;
    }

    get yearSelected(): number {
        return this.resultsConfig.getValue('year');
    }

    set yearSelected(val: number) {
        this.resultsConfig.setValue('year', val);
    }

}
