import {Location} from '@angular/common';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {form, Form, vp} from '@smmx/maria-commons';
import BigNumber from 'bignumber.js';
import {MessageService} from 'primeng';
import {ApiService} from 'src/app/services/api.service';
import {ResultsConfig} from 'src/app/services/results-config.service';
import {Session} from 'src/app/services/session.service';
import {UIControl} from 'src/app/services/ui-control.service';
import {Path} from 'src/app/utils/paths';

@Component({
    selector: 'app-path-index',
    templateUrl: './path-index.component.html',
    styleUrls: ['./path-index.component.less']
})
export class PathIndexComponent implements OnInit, OnDestroy {

    // ID
    componentId: string;

    // SERVICES
    path: Path;

    // INDEX
    hltPaths: Array<Array<any>>;
    hltPathsTable: any;
    hltPathsEvent: any;
    hltPathsLoading: boolean;
    hltPathsTotalRecords: number;
    getIndexPromise: any;
    hltPathData: any;

    // SEARCH
    searchMethods: any[];
    searchMethod: number;
    searchForms: Form[];

    // RESULTS SUBSCRIPTION
    resultsConfigUnsubscribe: any;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private resultsConfig: ResultsConfig,
        private location: Location,
        private apiService: ApiService,
        private messageService: MessageService,
        public session: Session,
        private uiControl: UIControl,
    ) {
        // ID
        this.componentId = 'cd07ead4eba04126a95ce27c7ed2808a';

        // UI
        this.uiControl.viewMode = 'catalog';

        // SERVICES
        this.path = new Path(route, router, location);

        // INDEX
        this.hltPaths = [];
        this.hltPathsTable = {
            fields: [
                {field: 'name', header: 'Name', sortable: true},
                {field: 'years', header: 'Years'},
                {field: 'intlumi_actual', header: 'Actual [fb^-1]', sortable: true},
                {field: 'intlumi_effective', header: 'Effective [fb^-1]', sortable: true},
                {field: '__options__', header: 'Options', sortable: false},
            ],
            data: []
        };
        this.hltPathsEvent = null;
        this.hltPathsLoading = false;
        this.hltPathsTotalRecords = 0;
        this.getIndexPromise = null;

        // SELECTED
        this._hltPath = null;
        this.hltPathData = null;

        // SEARCH
        this.searchMethods = [
            {label: 'By Name', value: 0},
            {label: 'By Seed', value: 1},
            {label: 'By Tag', value: 2},
        ];

        this.searchMethod = 0;

        this.searchForms = [
            form()
                .registerField(
                    'query',
                    vp()
                        .asString()
                        .notNull(),
                    '*'
                ),
            form()
                .registerField(
                    'seed',
                    vp()
                        .asString()
                        .notNull()
                ),
            form()
                .registerField(
                    'tag',
                    vp()
                        .asString()
                        .notNull()
                ),
        ];

        // SUBSCRIBE TO SEARCH CONFIG
        this.resultsConfigUnsubscribe = resultsConfig.addListener((field, oldValue, newValue) => {
            if (field === 'year') {
                this.refreshIndex();
            }
        });
    }

    // SELECTED
    _hltPath: string;

    get hltPath(): string {
        return this._hltPath;
    }

    set hltPath(hltPath: string) {
        this._hltPath = hltPath;

        if (hltPath) {
            // URL
            this.path.setParameter('select', hltPath);

            // DATA
            this.hltPathData = null;

            this.apiService.get(
                'hlt/paths/' + hltPath
            )
                .then(res => {
                    this.hltPathData = {...res};
                })
                .catch(err => {
                    this.hltPathData = null;
                });
        } else {
            // URL
            this.path.unsetParameter('select');

            // DATA
            this.hltPathData = null;
        }
    }

    ngOnInit(): void {
        // URL ARGS
        this.path.queryParams.subscribe(params => {
            if ('sMethod' in params) {
                const sMethod = vp()
                    .asString()
                    .notNull()
                    .map((method) => {
                        if (method === 'name') {
                            return 0;
                        } else if (method === 'seed') {
                            return 1;
                        } else if (method === 'tag') {
                            return 2;
                        } else {
                            return 0;
                        }
                    })
                    .apply(params.sMethod);

                this.searchMethod = sMethod;
            }

            if ('sQuery' in params) {
                const sQuery = vp()
                    .asString()
                    .notNull()
                    .apply(params.sQuery);

                this.searchForms[0].getField('query').value = sQuery;
            }

            if ('sSeed' in params) {
                const sSeed = vp()
                    .asString()
                    .notNull()
                    .apply(params.sSeed);

                this.searchForms[1].getField('seed').value = sSeed;
            }

            if ('sTag' in params) {
                const sTag = vp()
                    .asString()
                    .notNull()
                    .apply(params.sTag);

                this.searchForms[2].getField('tag').value = sTag;
            }

            // REFRESH
            this.refreshIndex();

            // ITEM
            if ('select' in params) {
                const select = vp()
                    .asString()
                    .notNull()
                    .apply(params.select);

                this.getIndexPromise.then((items) => {
                    this.hltPath = select;
                });
            }
        });
    }

    ngOnDestroy(): void {
        this.path.destroy();
        this.resultsConfigUnsubscribe();
    }

    refreshIndex(event = null): void {
        // CACHE PREVIOUS EVENT
        if (!event) {
            event = this.hltPathsEvent;
        } else {
            this.hltPathsEvent = event;
        }

        // RESET
        this.hltPathsTable.data = [];

        // BUILD SFIELDS
        let sFields = [];

        if (event && event.multiSortMeta) {
            for (let meta of event.multiSortMeta) {
                let order;

                if (meta.order === 1) {
                    order = 'ASC';
                } else {
                    order = 'DESC';
                }

                sFields.push(meta.field.replace('_view', '') + ':' + order);
            }
        }

        // BUILD QUERY
        this.path.unsetParameter('sMethod');
        this.path.unsetParameter('sQuery');
        this.path.unsetParameter('sSeed');
        this.path.unsetParameter('sTag');

        const request: any = {
            _pOffset: event ? event.first : 0,
            _pLimit: event ? event.rows : 50,
            _sFields: sFields,
        };

        if (this.searchMethod === 0) {
            request.query = this.searchForms[0].getValue('query');

            // SHORT-CIRCUIT
            if (!this.searchForms[0].allValid) {
                return;
            }

            // QUERY
            this.path.setParameter('sMethod', 'name');
            this.path.setParameter('sQuery', request.query);
        } else if (this.searchMethod === 1) {
            request.seed = this.searchForms[1].getValue('seed');

            // SHORT-CIRCUIT
            if (!this.searchForms[1].allValid) {
                return;
            }

            // QUERY
            this.path.setParameter('sMethod', 'seed');
            this.path.setParameter('sSeed', request.seed);
        } else if (this.searchMethod === 2) {
            request.tag = this.searchForms[2].getValue('tag');

            // SHORT-CIRCUIT
            if (!this.searchForms[2].allValid) {
                return;
            }

            // QUERY
            this.path.setParameter('sMethod', 'tag');
            this.path.setParameter('sTag', request.tag);
        }

        // FETCH
        this.hltPathsLoading = true;

        this.getIndexPromise = this.apiService.get(
            'hlt/paths',
            request
        )
            .then(res => {
                this.hltPaths = res.entries;

                this.hltPathsTable.data = res.entries.map((entry => {
                    entry.intlumi_actual = new BigNumber(entry.intlumi_actual);
                    entry.intlumi_effective = new BigNumber(entry.intlumi_effective);
                    return entry;
                }));

                if (event) {
                    if (this.hltPaths.length === event.rows) {
                        this.hltPathsTotalRecords = event.first + event.rows * 2;
                    }
                } else {
                    this.hltPathsTotalRecords = 100;
                }

                return this.hltPaths;
            })
            .finally(() => {
                this.hltPathsLoading = false;
            });
    }

    refreshSelected(): void {
        const tmp = this.hltPath;
        this.hltPath = null;

        setTimeout(() => {
            this.hltPath = tmp;
        }, 100);
    }

    showDetails(hltPath: string): void {
        this.hltPath = hltPath;
    }

}
