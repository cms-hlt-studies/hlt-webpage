import {ComponentFixture, TestBed} from '@angular/core/testing';

import {SeedIndexComponent} from './seed-index.component';

describe('SeedIndexComponent', () => {
    let component: SeedIndexComponent;
    let fixture: ComponentFixture<SeedIndexComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [SeedIndexComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(SeedIndexComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
