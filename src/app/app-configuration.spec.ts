import {TestBed} from '@angular/core/testing';

import {AppConfiguration} from './app-configuration';

describe('AppConfiguration', () => {
    let service: AppConfiguration;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(AppConfiguration);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
