export class CountdownLatch {

    _limit: number;
    _count: number;
    _closed: boolean;

    _callstack: Array<() => void>;

    constructor(limit: number) {
        this._limit = limit;
        this._count = 0;
        this._closed = false;

        this._callstack = [];
    }

    then(callback: () => void): CountdownLatch {
        if (this._closed) {
            callback();
        } else {
            this._callstack.push(callback);
        }

        return this;
    }

    countDown(): void {
        this._count += 1;

        if (this._limit <= this._count) {
            this._closed = true;

            this._callstack.forEach(function (callback) {
                callback();
            });

            this._callstack = null;
        }
    }

}
