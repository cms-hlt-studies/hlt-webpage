import {Location} from '@angular/common';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {form, Form, vp} from '@smmx/maria-commons';
import BigNumber from 'bignumber.js';
import {MessageService} from 'primeng';
import {ApiService} from 'src/app/services/api.service';
import {ResultsConfig} from 'src/app/services/results-config.service';
import {Session} from 'src/app/services/session.service';
import {UIControl} from 'src/app/services/ui-control.service';
import {stringToRGB} from 'src/app/utils/colors';
import {saveJSON} from 'src/app/utils/download';
import {Path} from 'src/app/utils/paths';


@Component({
    selector: 'app-prescale-summary',
    templateUrl: './prescale-summary.component.html',
    styleUrls: ['./prescale-summary.component.less']
})
export class PrescaleSummaryComponent implements OnInit, OnDestroy {

    // ID
    componentId: string;

    // SERVICES
    path: Path;

    // SEARCH
    searchMethods: any[];
    searchMethod: number;
    searchForms: Form[];

    // SUMMARY
    isEmpty: boolean;
    summary: any;
    timeBricks: any;

    // MERGE TOOL
    mergeToolObjects: any[];

    // RESULTS SUBSCRIPTION
    resultsConfigUnsubscribe: any;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private resultsConfig: ResultsConfig,
        private location: Location,
        private apiService: ApiService,
        private messageService: MessageService,
        public session: Session,
        private uiControl: UIControl,
    ) {
        // ID
        this.componentId = 'eb04a6b42d55424f938f606ca4fb86c7';

        // UI
        this.uiControl.viewMode = 'catalog';

        // SERVICES
        this.path = new Path(route, router, location);

        // SEARCH
        this.searchMethods = [
            {label: 'By Dataset', value: 0},
            {label: 'By Path', value: 1},
        ];

        this.searchMethod = 0;

        this.searchForms = [
            form()
                .registerField(
                    'dataset',
                    vp()
                        .asString()
                        .notNull(),
                ),
            form()
                .registerField(
                    'path',
                    vp()
                        .asString()
                        .notNull()
                ),
        ];

        // SUBSCRIBE TO SEARCH CONFIG
        this.resultsConfigUnsubscribe = resultsConfig.addListener((field, oldValue, newValue) => {
            if (field === 'year') {
                this.submit();
            }
        });

        // SUMMARY
        this.timeBricks = {};
        this.timeBricks.pathIndex = {};
        this.timeBricks.paths = [];

        // MERGE TOOL
        this.mergeToolObjects = [];
    }

    ngOnInit(): void {
        // URL ARGS
        this.path.queryParams.subscribe(params => {
            if ('sMethod' in params) {
                const sMethod = vp()
                    .asString()
                    .notNull()
                    .map((method) => {
                        if (method === 'dataset') {
                            return 0;
                        } else if (method === 'path') {
                            return 1;
                        } else {
                            return 0;
                        }
                    })
                    .apply(params.sMethod);

                this.searchMethod = sMethod;
            }

            if ('sDataset' in params) {
                const sDataset = vp()
                    .asString()
                    .notNull()
                    .apply(params.sDataset);

                this.searchForms[0].getField('dataset').value = sDataset;
            }

            if ('sPath' in params) {
                const sPath = vp()
                    .asString()
                    .notNull()
                    .apply(params.sPath);

                this.searchForms[1].getField('path').value = sPath;
            }

            // REFRESH
            this.submit();
        });
    }

    ngOnDestroy(): void {
        this.path.destroy();
        this.resultsConfigUnsubscribe();
    }

    submit(): void {
        if (this.searchMethod === 0) {
            // GET FIELDS
            const data = this.searchForms[0].getAllValues();

            // CHECKS
            if (!this.searchForms[0].areAllFieldsValid()) {
                return;
            }

            // UPDATE PATH
            this.path.setParameter('sMethod', 'dataset');
            this.path.setParameter('sDataset', data.dataset);

            // QUERY
            this.query({
                dataset_name: data.dataset,
            });
        } else if (this.searchMethod === 1) {
            // GET FIELDS
            const data = this.searchForms[1].getAllValues();

            // CHECKS
            if (!this.searchForms[1].areAllFieldsValid()) {
                return;
            }

            // UPDATE PATH
            this.path.setParameter('sMethod', 'path');
            this.path.setParameter('sPath', data.path);

            // QUERY
            this.query({
                path_name: data.path,
            });
        }

    }

    query(args: any): void {
        // GET YEAR
        const year = this.resultsConfig.getValue('year');

        // LOCK SCREEN
        const unlock = this.uiControl.lockScreen();

        // SUBMIT
        this.apiService.post(
            'hlt/tools/prescales',
            {
                dataset_name: args.dataset_name,
                path_name: args.path_name,
                from: year + '-01-01',
                to: year + '-12-31',
            }
        )
            .then(res => {
                // RESET
                this.summary = {...res};
                this.timeBricks.objectIndex = {};
                this.timeBricks.objectNameFromIndex = {};
                this.timeBricks.objects = [];
                this.timeBricks.rects = [];
                this.timeBricks.lumiAxis = [];
                this.mergeToolObjects = [];

                // BUILD TIME BRICK INDEX
                for (let i = 0; i <= 10; i++) {
                    this.timeBricks.lumiAxis.push(['8.5%', Math.round(i * 10 / 100 * res.runs[res.runs.length - 1].intlumi_end * 10) / 10]);
                }

                const maxIntLumi = res.runs[res.runs.length - 1].intlumi_end / 100;

                // BUILD OBJECT INDEX
                let consecutive = 0;

                for (const object of res.objects) {
                    this.timeBricks.objectIndex[object.name] = consecutive;
                    this.timeBricks.objectNameFromIndex[consecutive] = object.name;
                    this.timeBricks.objects.push([]);
                    this.timeBricks.rects.push([]);
                    consecutive = consecutive + 1;
                }

                // PROCESS RUNS
                for (const run of res.runs) {
                    for (const objectName of run.objects) {
                        const objectIndex = this.timeBricks.objectIndex[objectName];

                        if (this.timeBricks.objects[objectIndex].length > 0
                            && run.intlumi_start / maxIntLumi === this.timeBricks.objects[objectIndex][this.timeBricks.objects[objectIndex].length - 1][1]) {
                            this.timeBricks.objects[objectIndex][this.timeBricks.objects[objectIndex].length - 1][1] = run.intlumi_end / maxIntLumi;
                            this.timeBricks.rects[objectIndex][this.timeBricks.rects[objectIndex].length - 1][0] = ((
                                (run.intlumi_end / maxIntLumi - this.timeBricks.objects[objectIndex][this.timeBricks.objects[objectIndex].length - 1][0])) + '%');
                        } else {
                            if (this.timeBricks.objects[objectIndex].length > 0) {
                                this.timeBricks.rects[objectIndex].push([
                                    (run.intlumi_start / maxIntLumi - this.timeBricks.objects[objectIndex][this.timeBricks.objects[objectIndex].length - 1][1]) + '%',
                                    'transparent'
                                ]);
                            } else {
                                if (run.intlumi_start > 0) {
                                    this.timeBricks.rects[objectIndex].push([
                                        (run.intlumi_start / maxIntLumi) + '%',
                                        'transparent'
                                    ]);
                                }
                            }

                            this.timeBricks.objects[objectIndex].push([
                                run.intlumi_start / maxIntLumi, run.intlumi_end / maxIntLumi
                            ]);

                            this.timeBricks.rects[objectIndex].push([
                                (run.intlumi_end / maxIntLumi - run.intlumi_start / maxIntLumi) + '%',
                                '#' + stringToRGB(objectName.toLowerCase())
                            ]);
                        }
                    }
                }

                // CHECKS
                this.isEmpty = this.summary.objects.length === 0;

                // BUILD SUMMARY
                this.summary.title = args.dataset_name || args.path_name || 'Custom';
                this.summary.year = year;

                this.summary.objects.map(object => {
                    for (const entry of object.prls) {
                        entry.intlumi = new BigNumber(entry.intlumi);
                        entry.runnumbers = Object.keys(entry.rls);
                    }

                    object.prls.sort((e1, e2) => {
                        return e1.prescale - e2.prescale;
                    });

                    return object;
                });
            })
            .finally(() => {
                unlock();
            });
    }

    saveRLS(objectName: string, prls: any): void {
        saveJSON(`${objectName}_Prescale_${prls.prescale}.json`, prls.rls);
    }

    addObjectToMergeTool(objectName: string, prls: any): void {
        const objectIndex = this.mergeToolObjects.findIndex(el => el.name === objectName);

        if (objectIndex > -1) {
            this.mergeToolObjects.splice(objectIndex, 1);
        }

        const entry = {...prls};
        entry.name = objectName;
        this.mergeToolObjects.push(entry);
    }

    removeObjectFromMergeTool(objectName: string): void {
        const objectIndex = this.mergeToolObjects.findIndex(el => el.name === objectName);

        if (objectIndex > -1) {
            this.mergeToolObjects.splice(objectIndex, 1);
        }
    }

    mergeObjects(): void {
        // INIT
        const nObjectsToMerge = this.mergeToolObjects.length;

        // OUTPUT
        let mergedRLS = null;

        // SHORT-CIRCUIT
        if (nObjectsToMerge === 0) {
            return;
        }

        // SINGLE
        else if (nObjectsToMerge === 1) {
            mergedRLS = this.mergeToolObjects[0].rls;
        }

        // MERGE INTERVALS
        else if (nObjectsToMerge > 1) {
            // GET LIMITS
            const s1MergedLimits = {};

            this.mergeToolObjects.forEach(object => {
                Object.keys(object.rls).forEach(runnumber => {
                    object.rls[runnumber].forEach(LS => {
                        if (!s1MergedLimits[runnumber]) {
                            s1MergedLimits[runnumber] = [];
                        }

                        s1MergedLimits[runnumber].push(LS[0]);
                        s1MergedLimits[runnumber].push(LS[1]);
                    });
                });
            });

            // CONSTRUCT INTERVALS
            const s2MergedRLS = {};

            Object.keys(s1MergedLimits).forEach(runnumber => {
                // SORT LIMITS
                const limits = s1MergedLimits[runnumber];

                limits.sort((selected, compare) => {
                    if (selected > compare) {
                        return 1;
                    } else if (selected < compare) {
                        return -1;
                    }

                    return 0;
                });

                // INIT
                s2MergedRLS[runnumber] = [];

                // ADD INTERVALS
                let previousIndex = null;
                let previousLS = null;

                limits.forEach(index => {
                    if (previousIndex) {
                        const LS = [previousIndex, index];

                        if (!previousLS || !(previousLS[0] === LS[0] && previousLS[1] === LS[1])) {
                            s2MergedRLS[runnumber].push(LS);
                            previousLS = LS;
                        }
                    }

                    previousIndex = index;
                });
            });

            // OVERLAP INTERVALS
            const s3MergedRLS = {};

            Object.keys(s2MergedRLS).forEach(runnumber => {
                const mergedLS = s2MergedRLS[runnumber];

                mergedLS.forEach(mergedInterval => {
                    let overlappedObjects = 0;

                    this.mergeToolObjects.forEach(object => {
                        const objectLS = object.rls[runnumber];

                        if (!objectLS) {
                            return;
                        }

                        let overlapped = false;

                        objectLS.forEach(objectInterval => {
                            if (!overlapped && objectInterval[0] <= mergedInterval[0] && mergedInterval[1] <= objectInterval[1]) {
                                overlappedObjects += 1;
                                overlapped = true;
                            }
                        });
                    });

                    if (overlappedObjects === nObjectsToMerge) {
                        if (!s3MergedRLS[runnumber]) {
                            s3MergedRLS[runnumber] = [];
                        }

                        s3MergedRLS[runnumber].push(mergedInterval);
                    }
                });
            });

            // STITCH INTERVALS TOGETHER
            const s4MergedRLS = {};

            Object.keys(s3MergedRLS).forEach(runnumber => {
                const mergedLS = s3MergedRLS[runnumber];

                // INIT
                s4MergedRLS[runnumber] = [];

                // STITCH
                let previousLS = null;

                mergedLS.forEach(mergedInterval => {
                    if (!previousLS || previousLS[1] !== mergedInterval[0]) {
                        s4MergedRLS[runnumber].push(mergedInterval);
                        previousLS = mergedInterval;
                    } else if (previousLS && previousLS[1] === mergedInterval[0]) {
                        previousLS[1] = mergedInterval[1];
                    }
                });
            });

            // SET RLS
            mergedRLS = s4MergedRLS;
        }

        // SHORT-CIRCUIT
        if (Object.keys(mergedRLS).length === 0) {
            this.messageService.add({
                severity: 'error',
                summary: 'Error',
                detail: 'No overlaps found'
            });

            return;
        }

        // DOWNLOAD
        this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: 'Your merged output is ready!'
        });

        saveJSON(`merged.json`, mergedRLS);
    }

}
