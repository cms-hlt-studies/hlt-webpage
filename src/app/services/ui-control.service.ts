import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class UIControl {

    viewMode: string;
    screenLockCount: number;

    constructor() {
        this.viewMode = 'default';
        this.screenLockCount = 0;
    }

    get screenLocked(): boolean {
        return this.screenLockCount > 0;
    }

    lockScreen(): () => void {
        this.screenLockCount = this.screenLockCount + 1;

        return () => {
            this.screenLockCount = this.screenLockCount - 1;
        };
    }
}
