import {Component, OnInit} from '@angular/core';
import {vp} from '@smmx/maria-commons';
import moment from 'moment';
import {MessageService} from 'primeng/api';
import {DialogService} from 'primeng/dynamicdialog';
import {ConfirmationDialogComponent} from 'src/app/components/confirmation-dialog/confirmation-dialog.component';
import {FormDialogComponent} from 'src/app/components/form-dialog/form-dialog.component';
import {ApiService} from 'src/app/services/api.service';
import {UIControl} from 'src/app/services/ui-control.service';

@Component({
    selector: 'app-admin-panel',
    templateUrl: './admin-panel.component.html',
    styleUrls: ['./admin-panel.component.less']
})
export class AdminPanelComponent implements OnInit {

    users: Array<any>;
    apps: Array<any>;

    constructor(
        private apiService: ApiService,
        private dialogService: DialogService,
        private messageService: MessageService,
        private uiControl: UIControl,
    ) {
        // UI
        this.uiControl.viewMode = 'default';

        this.users = [];
        this.apps = [];
    }

    ngOnInit(): void {
        this.refreshUsers();
        this.refreshServices();
    }

    refreshUsers(): void {
        this.apiService.get(
            'security/users'
        )
            .then(res => {
                this.users = res.entries.map(entry => {
                    entry.timestamp = moment.unix(entry.timestamp).toLocaleString();

                    return entry;
                });
            });
    }

    refreshServices(): void {
        this.apiService.get(
            'security/apps'
        )
            .then(res => {
                this.apps = res.entries.map(entry => {
                    entry.timestamp = moment.unix(entry.timestamp).toLocaleString();

                    return entry;
                });
            });
    }

    onCreateUser(): void {
        const ref = this.dialogService.open(
            FormDialogComponent,
            {
                header: 'User Registration',
                baseZIndex: 1050,
                data: {
                    model: {
                        fields: [
                            {
                                name: 'user',
                                label: 'Username',
                                editor: {
                                    name: 'input',
                                },
                                valueProcessor: vp()
                                    .asString()
                                    .notNull()
                            },
                            {
                                name: 'roles',
                                label: 'Roles',
                                editor: {
                                    name: 'list',
                                    options: [
                                        {value: 'admin', label: 'Admin'},
                                        {value: 'maintainer', label: 'Maintainer'},
                                    ],
                                    multiple: true,
                                    bindLabel: 'label',
                                    bindValue: 'value',
                                },
                                valueProcessor: vp()
                                    .ifNull([])
                            }
                        ]
                    }
                }
            }
        );

        // PROCESS DIALOG RESPONSE
        ref.onClose.subscribe((data) => {
            // SHORT-CIRCUIT
            if (!data) {
                return;
            }

            // SUBMIT
            const lockHandler = this.uiControl.lockScreen();

            this.apiService.post(
                'security/users',
                data
            )
                .then(res => {
                    this.messageService.add({
                        severity: 'success',
                        summary: 'Success',
                        detail: 'Record created.'
                    });

                    this.refreshUsers();
                })
                .catch(err => {
                    this.messageService.add({
                        severity: 'error',
                        summary: 'Error',
                        detail: 'Record couldn\'t be created: ' + err.message
                    });
                })
                .finally(() => {
                    lockHandler();
                });
        });
    }

    onDeleteUser(record): void {
        // SHORT-CIRCUIT
        if (!record) {
            return;
        }

        // CONFIRM
        this.dialogService.open(
            ConfirmationDialogComponent,
            {
                header: 'Confirmation',
                baseZIndex: 1050,
                data: {
                    message: 'Are you sure you wish to delete user "' + record.id + '"?',
                    options: [
                        {value: 'yes', label: 'Yes'},
                        {value: 'no', label: 'No'},
                    ]
                }
            }
        ).onClose.subscribe(data => {
            // SHORT-CIRCUIT
            if (!data) {
                return;
            }

            if (data.option === 'yes') {
                // SUBMIT
                const lockHandler = this.uiControl.lockScreen();

                this.apiService.delete(
                    'security/users/' + record.id
                )
                    .then(res => {
                        this.messageService.add({
                            severity: 'success',
                            summary: 'Success',
                            detail: 'Record deleted.'
                        });

                        this.refreshUsers();
                    })
                    .catch(err => {
                        this.messageService.add({
                            severity: 'error',
                            summary: 'Error',
                            detail: 'Record couldn\'t be deleted: ' + err.message
                        });
                    })
                    .finally(() => {
                        lockHandler();
                    });
            }
        });
    }

    onCreateApp(): void {
        const ref = this.dialogService.open(
            FormDialogComponent,
            {
                header: 'App Registration',
                baseZIndex: 1050,
                data: {
                    model: {
                        fields: [
                            {
                                name: 'name',
                                label: 'Name',
                                editor: {
                                    name: 'input',
                                },
                                valueProcessor: vp()
                                    .asString()
                                    .notNull()
                            },
                            {
                                name: 'description',
                                label: 'Description',
                                editor: {
                                    name: 'input',
                                },
                                valueProcessor: vp()
                                    .asString()
                                    .notNull()
                            },
                            {
                                name: 'privileges',
                                label: 'Priviliges',
                                editor: {
                                    name: 'list',
                                    options: [
                                        {value: 'p-mine', label: 'Mine'},
                                        {value: 'p-tag', label: 'Tag'},
                                    ],
                                    multiple: true,
                                    bindLabel: 'label',
                                    bindValue: 'value',
                                },
                                valueProcessor: vp()
                                    .ifNull([])
                            }
                        ]
                    }
                }
            }
        );

        // PROCESS DIALOG RESPONSE
        ref.onClose.subscribe((data) => {
            // SHORT-CIRCUIT
            if (!data) {
                return;
            }

            // SUBMIT
            const lockHandler = this.uiControl.lockScreen();

            this.apiService.post(
                'security/apps',
                data
            )
                .then(res => {
                    this.messageService.add({
                        severity: 'success',
                        summary: 'Success',
                        detail: 'Record created.'
                    });

                    this.refreshServices();
                })
                .catch(err => {
                    this.messageService.add({
                        severity: 'error',
                        summary: 'Error',
                        detail: 'Record couldn\'t be created: ' + err.message
                    });
                })
                .finally(() => {
                    lockHandler();
                });
        });
    }

    onDeleteApp(record): void {
        // SHORT-CIRCUIT
        if (!record) {
            return;
        }

        // CONFIRM
        this.dialogService.open(
            ConfirmationDialogComponent,
            {
                header: 'Confirmation',
                baseZIndex: 1050,
                data: {
                    message: 'Are you sure you wish to delete app "' + record.name + '"?',
                    options: [
                        {value: 'yes', label: 'Yes'},
                        {value: 'no', label: 'No'},
                    ]
                }
            }
        ).onClose.subscribe(data => {
            // SHORT-CIRCUIT
            if (!data) {
                return;
            }

            if (data.option === 'yes') {
                // SUBMIT
                const lockHandler = this.uiControl.lockScreen();

                this.apiService.delete(
                    'security/apps/' + record.id
                )
                    .then(res => {
                        this.messageService.add({
                            severity: 'success',
                            summary: 'Success',
                            detail: 'Record deleted.'
                        });

                        this.refreshServices();
                    })
                    .catch(err => {
                        this.messageService.add({
                            severity: 'error',
                            summary: 'Error',
                            detail: 'Record couldn\'t be deleted: ' + err.message
                        });
                    })
                    .finally(() => {
                        lockHandler();
                    });
            }
        });
    }
}
