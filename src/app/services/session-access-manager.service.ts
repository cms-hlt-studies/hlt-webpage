import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {ApiService} from 'src/app/services/api.service';
import {Session} from 'src/app/services/session.service';

@Injectable({
    providedIn: 'root'
})
export class SessionAccessManager implements CanActivate {

    constructor(
        private router: Router,
        private apiService: ApiService,
        private session: Session
    ) {

    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        // CHECK SESSION
        return this.session.sync()
            .then(v => {
                let permittedRoles = next.data.permittedRoles;
                let error401URL = next.data.error401URL;

                if (permittedRoles === undefined) {
                    permittedRoles = ['anyone'];
                }

                if (error401URL === undefined) {
                    error401URL = '/401';
                }

                if (this.session.authenticated) {
                    if (permittedRoles.indexOf('anyone') > -1 || permittedRoles.indexOf('user') > -1) {
                        return true;
                    } else if (this.session.hasRoles(permittedRoles)) {
                        return true;
                    } else {
                        // REDIRECT
                        this.router.navigate([error401URL]);

                        // RETURN
                        return false;
                    }
                } else if (permittedRoles.indexOf('anyone') > -1 || permittedRoles.indexOf('no-user') > -1) {
                    return true;
                } else {
                    // REDIRECT
                    this.router.navigate(
                        ['/login'],
                        {
                            queryParams: {
                                returnUrl: state.url
                            }
                        }
                    );

                    // RETURN
                    return false;
                }
            })
            .catch(error => {
                // REDIRECT
                if (error.details.solution === 'login') {
                    this.router.navigate(
                        ['/login'],
                        {
                            queryParams: {
                                returnUrl: state.url
                            }
                        }
                    );
                }

                // RETURN
                return false;
            });
    }

}
