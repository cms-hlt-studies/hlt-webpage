export class Toggle {

    state: boolean;

    onMessage: string;
    offMessage: string;

    constructor(initState = false, onMessage = 'On', offMessage = 'Off') {
        this.state = initState;
        this.onMessage = onMessage;
        this.offMessage = offMessage;
    }

    get on() {
        return this.state;
    }

    set on(state) {
        this.state = state;
    }

    get off() {
        return !this.state;
    }

    set off(state) {
        this.state = !state;
    }

    get message() {
        if (this.state) {
            return this.onMessage;
        } else {
            return this.offMessage;
        }
    }

    toggle(state: boolean = null) {
        if (!state) {
            this.state = !this.state;
        } else {
            this.state = state;
        }
    }

}
