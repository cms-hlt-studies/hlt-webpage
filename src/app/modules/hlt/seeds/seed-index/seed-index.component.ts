import {Location} from '@angular/common';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {form, Form, vp} from '@smmx/maria-commons';
import BigNumber from 'bignumber.js';
import {MessageService} from 'primeng';
import {ApiService} from 'src/app/services/api.service';
import {ResultsConfig} from 'src/app/services/results-config.service';
import {Session} from 'src/app/services/session.service';
import {UIControl} from "src/app/services/ui-control.service";
import {Path} from 'src/app/utils/paths';

@Component({
    selector: 'app-seed-index',
    templateUrl: './seed-index.component.html',
    styleUrls: ['./seed-index.component.less']
})
export class SeedIndexComponent implements OnInit, OnDestroy {

    // ID
    componentId: string;

    // SERVICES
    path: Path;

    // INDEX
    seeds: Array<Array<any>>;
    seedsTable: any;
    seedsEvent: any;
    seedsLoading: boolean;
    seedsTotalRecords: number;
    getIndexPromise: any;
    seedData: any;

    // SEARCH
    searchMethods: any[];
    searchMethod: number;
    searchForms: Form[];

    // RESULTS SUBSCRIPTION
    resultsConfigUnsubscribe: any;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private resultsConfig: ResultsConfig,
        private location: Location,
        private apiService: ApiService,
        private messageService: MessageService,
        public session: Session,
        private uiControl: UIControl,
    ) {
        // ID
        this.componentId = '18103bdb66a2412284a19fe3af0f96a0';

        // UI
        this.uiControl.viewMode = 'catalog';

        // SERVICES
        this.path = new Path(route, router, location);

        // INDEX
        this.seeds = [];
        this.seedsTable = {
            fields: [
                {field: 'name', header: 'Name', sortable: true},
                {field: 'years', header: 'Years'},
                {field: 'intlumi_actual', header: 'Actual [fb^-1]', sortable: true},
                {field: 'intlumi_effective', header: 'Effective [fb^-1]', sortable: true},
                {field: '__options__', header: 'Options', sortable: false},
            ],
            data: []
        };
        this.seedsEvent = null;
        this.seedsLoading = false;
        this.seedsTotalRecords = 0;
        this.getIndexPromise = null;

        // SELECTED
        this._seed = null;
        this.seedData = null;

        // SEARCH
        this.searchMethods = [
            {label: 'By Name', value: 0},
            {label: 'By Period', value: 1},
            {label: 'By Tag', value: 2},
        ];

        this.searchMethod = 0;

        this.searchForms = [
            form()
                .registerField(
                    'query',
                    vp()
                        .asString()
                        .notNull(),
                    '*'
                ),
            form()
                .registerField(
                    'from',
                    vp()
                        .asInteger()
                        .notNull()
                        .check(year => 2009 <= year && year <= 2018, 'Only years between 2009 and 2018 can be used'),
                    2018
                )
                .registerField(
                    'to',
                    vp()
                        .asInteger()
                ),
            form()
                .registerField(
                    'tag',
                    vp()
                        .asString()
                        .notNull()
                ),
        ];

        // SUBSCRIBE TO SEARCH CONFIG
        this.resultsConfigUnsubscribe = resultsConfig.addListener((field, oldValue, newValue) => {
            if (field === 'year') {
                this.refreshIndex();
            }
        });
    }

    // SELECTED
    _seed: string;

    get seed(): string {
        return this._seed;
    }

    set seed(seed: string) {
        this._seed = seed;

        if (seed) {
            // URL
            this.path.setParameter('select', seed);

            // DATA
            this.seedData = null;

            this.apiService.get(
                'l1t/seeds/' + seed
            )
                .then(res => {
                    this.seedData = {...res};
                })
                .catch(err => {
                    this.seedData = null;
                });
        } else {
            // URL
            this.path.unsetParameter('select');

            // DATA
            this.seedData = null;
        }
    }

    ngOnInit(): void {
        // URL ARGS
        this.path.queryParams.subscribe(params => {
            if ('sMethod' in params) {
                const sMethod = vp()
                    .asString()
                    .notNull()
                    .map((method) => {
                        if (method === 'name') {
                            return 0;
                        } else if (method === 'period') {
                            return 1;
                        } else if (method === 'tag') {
                            return 2;
                        } else {
                            return 0;
                        }
                    })
                    .apply(params.sMethod);

                this.searchMethod = sMethod;
            }

            if ('sQuery' in params) {
                const sQuery = vp()
                    .asString()
                    .notNull()
                    .apply(params.sQuery);

                this.searchForms[0].getField('query').value = sQuery;
            }

            if ('sFrom' in params) {
                const sFrom = vp()
                    .asInteger()
                    .notNull()
                    .apply(params.sFrom);

                this.searchForms[1].getField('from').value = sFrom;
            }

            if ('sTo' in params) {
                const sTo = vp()
                    .asInteger()
                    .notNull()
                    .apply(params.sTo);

                this.searchForms[1].getField('to').value = sTo;
            }

            if ('sTag' in params) {
                const sTag = vp()
                    .asString()
                    .notNull()
                    .apply(params.sTag);

                this.searchForms[2].getField('tag').value = sTag;
            }

            // REFRESH
            this.refreshIndex();

            // ITEM
            if ('select' in params) {
                const select = vp()
                    .asString()
                    .notNull()
                    .apply(params.select);

                this.getIndexPromise.then((items) => {
                    this.seed = select;
                });
            }
        });
    }

    ngOnDestroy(): void {
        this.path.destroy();
        this.resultsConfigUnsubscribe();
    }

    refreshIndex(event = null): void {
        // CACHE PREVIOUS EVENT
        if (!event) {
            event = this.seedsEvent;
        } else {
            this.seedsEvent = event;
        }

        // RESET
        this.seedsTable.data = [];

        // BUILD SFIELDS
        let sFields = [];

        if (event && event.multiSortMeta) {
            for (let meta of event.multiSortMeta) {
                let order;

                if (meta.order === 1) {
                    order = 'ASC';
                } else {
                    order = 'DESC';
                }

                sFields.push(meta.field.replace('_view', '') + ':' + order);
            }
        }

        // BUILD QUERY
        this.path.unsetParameter('sMethod');
        this.path.unsetParameter('sQuery');
        this.path.unsetParameter('sFrom');
        this.path.unsetParameter('sTo');
        this.path.unsetParameter('sSeed');
        this.path.unsetParameter('sTag');

        const request: any = {
            _pOffset: event ? event.first : 0,
            _pLimit: event ? event.rows : 50,
            _sFields: sFields,
        };

        if (this.searchMethod === 0) {
            request.query = this.searchForms[0].getValue('query');

            // SHORT-CIRCUIT
            if (!this.searchForms[0].allValid) {
                return;
            }

            // QUERY
            this.path.setParameter('sMethod', 'name');
            this.path.setParameter('sQuery', request.query);
        } else if (this.searchMethod === 1) {
            request.from = this.searchForms[1].getValue('from');
            request.to = this.searchForms[1].getValue('to');

            // SHORT-CIRCUIT
            if (!this.searchForms[1].allValid) {
                return;
            }

            // QUERY
            this.path.setParameter('sMethod', 'period');
            this.path.setParameter('sFrom', request.from);

            if (request.to) {
                this.path.setParameter('sTo', request.to);
            }

            // NORMALIZE
            request.to = request.to ? request.to + '-12-31' : request.from + '-12-31';
            request.from = request.from + '-01-01';
        } else if (this.searchMethod === 2) {
            request.tag = this.searchForms[2].getValue('tag');

            // SHORT-CIRCUIT
            if (!this.searchForms[2].allValid) {
                return;
            }

            // QUERY
            this.path.setParameter('sMethod', 'tag');
            this.path.setParameter('sTag', request.tag);
        }

        // FETCH
        this.seedsLoading = true;

        this.getIndexPromise = this.apiService.get(
            'l1t/seeds',
            request
        )
            .then(res => {
                this.seeds = res.entries;

                this.seedsTable.data = res.entries.map((entry => {
                    entry.intlumi_actual = new BigNumber(entry.intlumi_actual);
                    entry.intlumi_effective = new BigNumber(entry.intlumi_effective);
                    return entry;
                }));

                if (event) {
                    if (this.seeds.length === event.rows) {
                        this.seedsTotalRecords = event.first + event.rows * 2;
                    }
                } else {
                    this.seedsTotalRecords = 100;
                }

                return this.seeds;
            })
            .finally(() => {
                this.seedsLoading = false;
            });
    }

    refreshSelected(): void {
        const tmp = this.seed;
        this.seed = null;

        setTimeout(() => {
            this.seed = tmp;
        }, 100);
    }

    showDetails(seed: string): void {
        this.seed = seed;
    }

}
