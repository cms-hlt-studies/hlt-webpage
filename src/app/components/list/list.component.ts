import {Component, ContentChild, ElementRef, EventEmitter, forwardRef, Input, OnInit, Output} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

export class Item {

    data: any;
    selected: boolean;

    constructor(data: any, selected: boolean = false) {
        this.data = data;
        this.selected = selected;
    }

    toggle(): boolean {
        this.selected = !this.selected;
        return this.selected;
    }

}

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.less'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ListComponent),
            multi: true
        }
    ],
})
export class ListComponent implements OnInit, ControlValueAccessor {

    // COMPONENT
    @Input()
    bindValue: string;

    @Input()
    bindLabel: string;

    paginationEvent;

    @Input()
    rows: number;

    @Input()
    totalRecords: number;

    @Input()
    loading: boolean;

    @Output()
    lazyLoad = new EventEmitter();

    @ContentChild('itemTemplate')
    itemTemplate: ElementRef;

    // SELECT
    selectedItem = null;

    constructor() {
        // COMPONENT
        this.bindValue = null;
        this.bindLabel = null;
        this.paginationEvent = null;

        // INIT
        this.loading = false;

        // ITEMS
        this._items = [];

        // SELECT
        this.selectedItem = null;
    }

    // ITEMS
    _items: Array<Item>;

    get items(): Array<any> {
        return this._items;
    }

    @Input()
    set items(newItems: Array<any>) {
        // NORMALIZE ITEMS
        if (!newItems) {
            newItems = [];
        }

        // SAVE SELECTED
        let selected = null;

        if (this.selectedItem) {
            selected = this.selectedItem.data[this.bindValue];
        }

        // GET ITEMS
        this._items = newItems.map((data) => new Item(data));

        // SELECT
        this.select(selected);
    }

    // FORM CONTROL
    onChange: any = () => {
    }

    onTouched: any = () => {
    }

    ngOnInit(): void {

    }

    registerOnChange(fn): void {
        this.onChange = fn;
    }

    registerOnTouched(fn): void {
        this.onTouched = fn;
    }

    pushValue(): void {
        if (this.selectedItem) {
            this.onChange(this.selectedItem.data[this.bindValue]);
            this.onTouched();
        } else {
            this.onChange(null);
            this.onTouched();
        }
    }

    writeValue(selected: any): void {
        if (!selected) {
            selected = null;
        }

        this.select(selected);
    }

    // SELECT LIST
    select(selected): void {
        // RESET
        if (this.selectedItem) {
            this.selectedItem.selected = false;
            this.selectedItem = null;
        }

        // SELECT
        if (selected) {
            let newSelectedItem = null;

            for (const item of this._items) {
                if (selected === item.data[this.bindValue]) {
                    newSelectedItem = item;
                    break;
                }
            }

            if (newSelectedItem) {
                this.selectedItem = newSelectedItem;
                this.selectedItem.selected = true;
            }
        }
    }

    // COMPONENT
    onSearch(): void {
        this.onLoad();
    }

    onLoad(event = null): void {
        if (event) {
            this.paginationEvent = event;
        } else {
            event = this.paginationEvent;
        }

        this.lazyLoad.emit({
            first: event ? event.first : 0,
            rows: event ? event.rows : 50,
        });
    }

    onToggle(item: Item): void {
        // TOGGLE
        if (item !== this.selectedItem) {
            if (item.toggle()) {
                if (this.selectedItem) {
                    this.selectedItem.selected = false;
                }

                this.selectedItem = item;
            }
        } else if (!item.toggle()) {
            this.selectedItem = null;
        }

        // PUSH
        this.pushValue();
    }

}
