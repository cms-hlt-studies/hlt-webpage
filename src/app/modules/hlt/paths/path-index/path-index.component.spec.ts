import {ComponentFixture, TestBed} from '@angular/core/testing';

import {PathIndexComponent} from './path-index.component';

describe('PathIndexComponent', () => {
    let component: PathIndexComponent;
    let fixture: ComponentFixture<PathIndexComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [PathIndexComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(PathIndexComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
