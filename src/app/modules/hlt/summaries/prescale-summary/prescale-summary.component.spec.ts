import {ComponentFixture, TestBed} from '@angular/core/testing';

import {PrescaleSummaryComponent} from 'src/app/modules/hlt/summaries/prescale-summary/prescale-summary.component';

describe('PrescaleSummaryComponent', () => {
    let component: PrescaleSummaryComponent;
    let fixture: ComponentFixture<PrescaleSummaryComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [PrescaleSummaryComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(PrescaleSummaryComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
