function hashCode(str): number { // java String#hashCode
    let hash = 0;

    for (let i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }

    return hash;
}

function intToRGB(i: number): string {
    const c = (i & 0x00FFFFFF)
        .toString(16)
        .toUpperCase();

    return '00000'.substring(0, 6 - c.length) + c;
}

export function rgbToLuminance(rgb: string): number {
    const R = parseInt(rgb.substr(0, 2), 16) / 255;
    const G = parseInt(rgb.substr(2, 2), 16) / 255;
    const B = parseInt(rgb.substr(4, 2), 16) / 255;

    // https://stackoverflow.com/questions/596216/formula-to-determine-brightness-of-rgb-color
    return Math.sqrt(0.299 * Math.pow(R, 2) + 0.587 * Math.pow(G, 2) + 0.114 * Math.pow(B, 2));
}

export function stringToRGB(str: string): string {
    return intToRGB(hashCode(str));
}
