import {OverlayRef} from '@angular/cdk/overlay';
import {TemplateRef, Type} from '@angular/core';
import {Subject} from 'rxjs';

export interface DialogCloseEvent<R> {
    type: 'backdropClick' | 'close';
    result: R;
}

// R = Response Data Type
// T = Data passed to Modal Type
export class DialogRef<R = any, T = any> {
    onClose = new Subject<DialogCloseEvent<R>>();

    constructor(
        public overlay: OverlayRef,
        public content: string | TemplateRef<any> | Type<any>,
        public data: T // pass data to modal i.e. FormData
    ) {
        overlay.backdropClick().subscribe(() => {
            this._close('backdropClick', null);
        });
    }

    close(data?: R): void {
        this._close('close', data);
    }

    private _close(type: 'backdropClick' | 'close', result: R): void {
        this.overlay.dispose();
        this.onClose.next({
            type,
            result
        });

        this.onClose.complete();
    }
}
