import {Component, OnInit, TemplateRef, Type} from '@angular/core';
import {DialogRef} from 'src/app/services/dialog/dialog.model';

@Component({
    selector: 'app-dialog-root',
    templateUrl: './dialog.component.html',
    styleUrls: ['./dialog.component.less']
})
export class DialogComponent implements OnInit {

    contentType: 'template' | 'string' | 'component';
    content: string | TemplateRef<any> | Type<any>;
    context;

    constructor(private dialogRef: DialogRef) {
    }

    ngOnInit(): void {
        this.content = this.dialogRef.content;

        if (typeof this.content === 'string') {
            this.contentType = 'string';
        } else if (this.content instanceof TemplateRef) {
            this.contentType = 'template';

            this.context = {
                close: this.dialogRef.close.bind(this.dialogRef)
            };
        } else {
            this.contentType = 'component';
        }
    }

    close(): void {
        this.dialogRef.close(null);
    }

}
