import {ComponentFixture, TestBed} from '@angular/core/testing';

import {RunsSummaryComponent} from './runs-summary.component';

describe('SummaryComponent', () => {
    let component: RunsSummaryComponent;
    let fixture: ComponentFixture<RunsSummaryComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [RunsSummaryComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(RunsSummaryComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
