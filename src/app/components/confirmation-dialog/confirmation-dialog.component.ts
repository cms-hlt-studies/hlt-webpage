import {Component, OnInit} from '@angular/core';
import {DialogService, DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';

@Component({
    selector: 'app-confirmation-dialog',
    templateUrl: './confirmation-dialog.component.html',
    styleUrls: ['./confirmation-dialog.component.less'],
    providers: [
        DialogService
    ],
})
export class ConfirmationDialogComponent implements OnInit {

    constructor(
        private dialogRef: DynamicDialogRef,
        public dialogConfig: DynamicDialogConfig
    ) {

    }

    ngOnInit(): void {

    }

    close(value): void {
        this.dialogRef.close({
            option: value
        });
    }

}
