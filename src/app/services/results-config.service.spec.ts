import {TestBed} from '@angular/core/testing';

import {ResultsConfig} from 'src/app/services/results-config.service';

describe('GlobalConfigService', () => {
    let service: ResultsConfig;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(ResultsConfig);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
