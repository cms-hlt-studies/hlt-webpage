import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {vp} from '@smmx/maria-commons';
import BigNumber from "bignumber.js";
import {MessageService} from 'primeng/api';
import {DialogService} from 'primeng/dynamicdialog';
import {ConfirmationDialogComponent} from 'src/app/components/confirmation-dialog/confirmation-dialog.component';
import {FormDialogComponent} from 'src/app/components/form-dialog/form-dialog.component';
import {ApiService} from 'src/app/services/api.service';
import {Session} from 'src/app/services/session.service';
import {UIControl} from 'src/app/services/ui-control.service';
import {rgbToLuminance, stringToRGB} from 'src/app/utils/colors';

@Component({
    selector: 'app-dataset-view',
    templateUrl: './dataset-view.component.html',
    styleUrls: ['./dataset-view.component.less']
})
export class DatasetViewComponent implements OnInit {

    // COMPONENT
    componentId: string;

    // ANGULAR
    @Output()
    update = new EventEmitter();

    // DATA
    tags: any[];
    sumaries: any[];
    pathsTable: any;

    constructor(
        private router: Router,
        private dialogService: DialogService,
        private messageService: MessageService,
        private apiService: ApiService,
        private uiControl: UIControl,
        public session: Session
    ) {
        // COMPONENT
        this.componentId = '338610fe1d804ef5a8104b43c2f1d606';

        // DATA
        this.tags = [];
        this.sumaries = [];
        this.pathsTable = {
            fields: [
                {field: 'name', header: 'Name'},
                {field: 'from', header: 'From'},
                {field: 'to', header: 'To'},
                {field: 'intlumi_actual', header: 'Actual [fb^-1]'},
                {field: 'intlumi_effective', header: 'Effective [fb^-1]'},
            ],
            data: []
        };
    }

    get isMaintainer(): boolean {
        if (!this.session.roles) {
            return false;
        }

        return this.session.roles.indexOf('maintainer') > -1;
    }

    // VIEW
    _datasetData: any;

    get datasetData(): any {
        return this._datasetData;
    }

    @Input()
    set datasetData(datasetData: any) {
        this._datasetData = datasetData;

        // RESET
        this.tags = [null];
        this.sumaries = [];
        this.pathsTable.data = [];

        // SHORT-CIRCUIT
        if (!this._datasetData) {
            return;
        }

        // LOAD
        this._datasetData.tags.forEach((tag) => {
            const background = stringToRGB(tag);
            const luminance = rgbToLuminance(background);

            this.tags.push({
                name: tag,
                background: '#' + background,
                color: luminance >= .5 ? '#000' : '#fff',
            });
        });

        this._datasetData.summaries.forEach(summary => {
            const background = stringToRGB(summary.intlumi_effective.toString());
            const luminance = rgbToLuminance(background);

            this.sumaries.push({
                year: summary.year,
                intlumi: {
                    actual: new BigNumber(summary.intlumi_actual),
                    effective: new BigNumber(summary.intlumi_effective)
                },
                background: '#' + background,
                color: luminance >= .5 ? '#000' : '#fff',
            });
        });

        this.pathsTable.data = datasetData.paths.map(record => {
            record.intlumi_actual = new BigNumber(record.intlumi_actual);
            record.intlumi_effective = new BigNumber(record.intlumi_effective);
            return record;
        });
    }

    ngOnInit(): void {

    }

    refresh(): void {
        this.update.emit(null);
    }

    onUpdate(): void {
        const ref = this.dialogService.open(
            FormDialogComponent,
            {
                header: 'Update Comments',
                baseZIndex: 1050,
                data: {
                    model: {
                        fields: [
                            {
                                name: 'comments',
                                label: 'Comments',
                                editor: {
                                    name: 'textarea',
                                },
                                initValue: this.datasetData.comments || '',
                                valueProcessor: vp()
                                    .asString()
                            },
                        ]
                    }
                }
            }
        );

        // PROCESS DIALOG RESPONSE
        ref.onClose.subscribe((data) => {
            // SHORT-CIRCUIT
            if (!data) {
                return;
            }

            // SUBMIT
            const lockHandler = this.uiControl.lockScreen();

            this.apiService.put(
                'hlt/datasets/' + this.datasetData.id,
                data
            )
                .then(res => {
                    this.messageService.add({
                        severity: 'success',
                        summary: 'Success',
                        detail: 'Record created.'
                    });

                    this.refresh();
                })
                .catch(err => {
                    this.messageService.add({
                        severity: 'error',
                        summary: 'Error',
                        detail: 'Record couldn\'t be created: ' + err.message
                    });
                })
                .finally(() => {
                    lockHandler();
                });
        });
    }

    onSearchByTag(tag: string): void {
        this.router.navigate(
            ['/datasets'], {queryParams: {sMethod: 'tag', sTag: tag}}
        );
    }

    onAddTag(): void {
        const ref = this.dialogService.open(
            FormDialogComponent,
            {
                header: 'Add Tag',
                baseZIndex: 1050,
                data: {
                    model: {
                        fields: [
                            {
                                name: 'tag',
                                label: 'Tag',
                                editor: {
                                    name: 'input',
                                },
                                valueProcessor: vp()
                                    .asString()
                                    .notNull()
                            },
                        ]
                    }
                }
            }
        );

        // PROCESS DIALOG RESPONSE
        ref.onClose.subscribe((data) => {
            // SHORT-CIRCUIT
            if (!data) {
                return;
            }

            // SUBMIT
            const lockHandler = this.uiControl.lockScreen();

            this.apiService.post(
                'hlt/datasets/' + this.datasetData.id + '/tags',
                {
                    tag: data.tag
                }
            )
                .then(res => {
                    this.messageService.add({
                        severity: 'success',
                        summary: 'Success',
                        detail: 'Record created.'
                    });

                    this.refresh();
                })
                .catch(err => {
                    this.messageService.add({
                        severity: 'error',
                        summary: 'Error',
                        detail: 'Record couldn\'t be created: ' + err.message
                    });
                })
                .finally(() => {
                    lockHandler();
                });
        });
    }

    onRemoveTag(tag): void {
        // SHORT-CIRCUIT
        if (!tag) {
            return;
        }

        // CONFIRM
        this.dialogService.open(
            ConfirmationDialogComponent,
            {
                header: 'Confirmation',
                baseZIndex: 1050,
                data: {
                    message: 'Are you sure you wish to delete tag "' + tag.name + '"?',
                    options: [
                        {value: 'yes', label: 'Yes'},
                        {value: 'no', label: 'No'},
                    ]
                }
            }
        ).onClose.subscribe(data => {
            // SHORT-CIRCUIT
            if (!data) {
                return;
            }

            if (data.option === 'yes') {
                // SUBMIT
                const lockHandler = this.uiControl.lockScreen();

                this.apiService.delete(
                    'hlt/datasets/' + this.datasetData.id + '/tags/' + tag.name
                )
                    .then(res => {
                        this.messageService.add({
                            severity: 'success',
                            summary: 'Success',
                            detail: 'Record deleted.'
                        });

                        this.refresh();
                    })
                    .catch(err => {
                        this.messageService.add({
                            severity: 'error',
                            summary: 'Error',
                            detail: 'Record couldn\'t be deleted: ' + err.message
                        });
                    })
                    .finally(() => {
                        lockHandler();
                    });
            }
        });
    }

}
