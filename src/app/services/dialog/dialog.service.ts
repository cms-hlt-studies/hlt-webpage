import {Overlay, OverlayConfig} from '@angular/cdk/overlay';
import {ComponentPortal} from '@angular/cdk/portal';
import {Injectable, Injector, TemplateRef, Type} from '@angular/core';
import {DialogComponent} from 'src/app/services/dialog/dialog.component';
import {DialogRef} from 'src/app/services/dialog/dialog.model';

@Injectable({
    providedIn: 'root'
})
export class DialogService {

    constructor(private overlay: Overlay, private injector: Injector) {
    }

    open<R = any, T = any>(
        content: string | TemplateRef<any> | Type<any>,
        data: T
    ): DialogRef<R> {
        const configs = new OverlayConfig({
            hasBackdrop: true,
            panelClass: ['hlt-dialog', 'is-active'],
            backdropClass: 'hlt-dialog-backdrop'
        });

        const overlayRef = this.overlay.create(configs);

        const dialogRef = new DialogRef<R, T>(overlayRef, content, data);

        const injector = Injector.create({
            parent: this.injector,
            providers: [
                {
                    provide: DialogRef, useValue: dialogRef
                }
            ]
        });

        overlayRef.attach(
            new ComponentPortal(DialogComponent, null, injector)
        );

        return dialogRef;
    }

}
