import {Injectable} from '@angular/core';
import url from 'url';
import {AppConfiguration} from '../app-configuration';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    serverUrl: string;
    reachable: boolean;
    listeners: Array<any>;
    interceptors: Array<(message: Response) => void>;

    baseHeaders: () => any;

    constructor() {
        this.serverUrl = AppConfiguration.root.api.url;
        this.reachable = false;
        this.listeners = [];
        this.interceptors = [];

        this.baseHeaders = () => ({});
    }

    addListener(listener: any): void {
        this.listeners.push(listener);
    }

    addInterceptor(interceptor: any): void {
        this.interceptors.push(interceptor);
    }

    _fireEvent(event: string, ...args: any[]): void {
        setTimeout(
            () => {
                this.listeners.forEach(listener => {
                    if (listener[event] !== undefined) {
                        listener[event](...args);
                    }
                });
            },
            0
        );
    }

    _resolveEndpoint(endpoint: string, query: any = null): string {
        endpoint = url.resolve(this.serverUrl, endpoint);

        if (query) {
            // BUILD
            const searchParams = new URLSearchParams(query);

            // APPEND
            endpoint = endpoint + '?' + searchParams.toString();
        }

        return endpoint;
    }

    async get(endpoint: string, request: any = null, headers: any = {}): Promise<any> {
        const config = {
            method: 'GET',
            credentials: 'include',
            headers,
            redirect: 'follow',
            referrer: 'no-referrer',
        };

        return this._fetch(endpoint, config, request);
    }

    async post(endpoint: string, request: any = {}, files: Array<any> = [], headers: any = {}): Promise<any> {
        const config = {
            method: 'POST',
            cache: 'no-cache',
            credentials: 'include',
            headers,
            redirect: 'follow',
            referrer: 'no-referrer',
        };

        return this._fetch(endpoint, config, request, files);
    }

    async put(endpoint: string, request: any = {}, files: Array<any> = [], headers: any = {}): Promise<any> {
        const config = {
            method: 'PUT',
            cache: 'no-cache',
            credentials: 'include',
            headers,
            redirect: 'follow',
            referrer: 'no-referrer',
        };

        return this._fetch(endpoint, config, request, files);
    }

    async delete(endpoint: string, request: any = {}, headers: any = {}): Promise<any> {
        const config = {
            method: 'DELETE',
            cache: 'no-cache',
            credentials: 'include',
            headers,
            redirect: 'follow',
            referrer: 'no-referrer',
        };

        return this._fetch(endpoint, config, request);
    }

    async blob(endpoint: string, request: any = {}, headers: any = {}): Promise<any> {
        const config = {
            method: 'POST',
            cache: 'no-cache',
            credentials: 'include',
            headers,
            redirect: 'follow',
            referrer: 'no-referrer',
        };

        return this._fetch(endpoint, config, request);
    }

    _fetch(
        endpoint: string,
        config: any,
        request: any = null,
        files: Array<any> = []
    ): Promise<any> {
        // CONTENT
        let body: any;

        // HEADERS
        const baseHeaders: any = this.baseHeaders();

        // INJECT CORS
        baseHeaders['Access-Control-Allow-Origin'] = this.serverUrl;

        // SET HEADER AND BODY
        if (config.method === 'GET') {
            endpoint = this._resolveEndpoint(endpoint, request);
        } else if (files.length === 0) {
            // URL
            endpoint = url.resolve(this.serverUrl, endpoint);

            // HEADER
            baseHeaders['Content-Type'] = 'application/json';

            // BODY
            body = (request ? JSON.stringify(request) : undefined);
        } else {
            // URL
            endpoint = url.resolve(this.serverUrl, endpoint);

            // BUILD FORM DATA
            const formData = new FormData();

            if (request) {
                formData.append('request', JSON.stringify(request));
            }

            for (const f of files) {
                formData.append(f.name, f.file, f.filename || f.file.name);
            }

            // BODY
            body = formData;
        }

        // BUILD REQUEST
        config.headers = this._mergeObjects(baseHeaders, config.headers);
        config.body = body;

        // FETCH
        let reachableTemp = this.reachable;

        return fetch(
            endpoint,
            config
        )
            .then(async res => {
                // IS REACHABLE
                reachableTemp = true;

                // INTERCEPT
                this.interceptors.forEach(interceptor => {
                    interceptor(res);
                });

                // CHECK CONTENT TYPE
                const resContentType = res.headers.get('Content-Type');

                let isJSON = false;
                isJSON = isJSON || resContentType === null;
                isJSON = isJSON || resContentType === undefined;
                isJSON = isJSON || resContentType === 'text/plain';
                isJSON = isJSON || resContentType === 'application/json';

                if (isJSON) {
                    // READ RESPONSE
                    const jsonRes = await res.json();

                    const message = new APIMessage(
                        jsonRes
                    );

                    // CHECK ERRORS
                    if (message.iserror) {
                        throw APIError.fromAPIMessage(message);
                    } else {
                        return message.payload;
                    }
                } else {
                    return res.blob();
                }
            })
            .catch(err => {
                // CONVERT TO API ERROR
                if (!(err instanceof APIError)) {
                    // NOT REACHABLE
                    reachableTemp = false;

                    // THROW ERROR
                    err = new APIError(-1, -1, err.message, {});
                }

                // FIRE
                this._fireEvent('onError', err);

                // THROW
                throw err;
            })
            .finally(() => {
                if (this.reachable !== reachableTemp) {
                    this.reachable = reachableTemp;

                    if (this.reachable) {
                        this._fireEvent('onReachable');
                    } else {
                        this._fireEvent('onUnreachable');
                    }
                }
            });
    }

    _mergeObjects(...objs: any[]): any {
        const merged: any = {};

        objs.forEach(obj => {
            Object.getOwnPropertyNames(obj).forEach(property => {
                merged[property] = obj[property];
            });
        });

        return merged;
    }
}

export class APIMessage {

    header: any;
    payload: any;

    iserror: boolean;

    constructor(message: string) {
        this.header = this._opt('header', {}, message);
        this.payload = this._opt('payload', {}, message);
        this.iserror = this._opt('__iserror__', false, this.header);
    }

    _opt(key: string, def: any, map: any): any {
        if (key in map) {
            return map[key];
        } else {
            return def;
        }
    }
}

export class APIError extends Error {

    code: number;
    status: number;
    message: string;
    details: any;

    constructor(code: number, status: number, message: string, details: any) {
        super('ERROR ' + code + ': ' + message);

        Object.setPrototypeOf(this, APIError.prototype);

        this.code = code;
        this.status = status;
        this.message = message;
        this.details = details;
    }

    static fromAPIMessage(message: APIMessage): APIError {
        return new this(
            message.payload.code,
            message.payload.status,
            message.payload.message,
            message.payload.details
        );
    }
}
