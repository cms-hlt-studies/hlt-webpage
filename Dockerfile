FROM nginx:latest

# INSTALL
COPY ./dist/HLTAnalysisTool /usr/share/nginx/html/
COPY ./docker/nginx.conf /etc/nginx/nginx.conf
RUN touch /var/run/nginx.pid

# SUPPORT RUNNING AS ARBITRARY USER WHICH BELONGS TO THE ROOT GROUP
RUN chmod g+rwx /var/cache/nginx /var/run/nginx.pid /var/log/nginx && chmod -R g+w /etc/nginx

# EXPOSE
EXPOSE 8080
