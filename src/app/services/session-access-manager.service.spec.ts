import {TestBed} from '@angular/core/testing';

import {SessionAccessManager} from './session-access-manager.service';

describe('SessionAccessManager', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: SessionAccessManager = TestBed.get(SessionAccessManager);
        expect(service).toBeTruthy();
    });
});
