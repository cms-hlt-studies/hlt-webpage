// SAVE
export function saveFile(filename, blob): void {
    // CREATE URL
    const url = window.URL.createObjectURL(blob);

    // CREATE ELEMENT
    const tempElement = document.createElement('a');
    document.body.appendChild(tempElement);
    tempElement.style.display = 'none';
    tempElement.download = filename;
    tempElement.href = url;

    // CLICK
    tempElement.click();

    // UNDO ALL
    window.URL.revokeObjectURL(url);
    tempElement.remove();
}

// FORMATS
export function saveCSV(filename, fields, rows): void {
    // INIT TEXT
    let text = '';

    // BUILD HEADER
    text += fields.map(field => `"${field}"`).join(',') + '\n';

    rows.forEach((row) => {
        // CHECK
        if (row.length !== fields.length) {
            throw new Error('Row length doesn\'t match number fields.');
        }

        // BUILD ROW
        text += row.map(value => {
            if (isNaN(value)) {
                return `"${value}"`;
            } else {
                return `${value}`;
            }
        }).join(',') + '\n';
    });

    // SAVE
    const blob = new Blob([text], {
        type: 'text/csv',
        endings: 'native'
    });

    saveFile(filename, blob);
}

export function saveJSON(filename, data): void {
    // SAVE
    const json = JSON.stringify(data);

    const blob = new Blob([json], {
        type: 'text/json',
        endings: 'native'
    });

    saveFile(filename, blob);
}
